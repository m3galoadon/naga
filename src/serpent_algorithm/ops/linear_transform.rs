// NAGA - megaloadon - 2023

/// A trait defining linear transformation methods for cryptographic operations.
///
/// The `LinearTransformation` trait provides methods for applying and inverting linear
/// transformations on data structures. This trait is commonly used in cryptographic
/// operations such as block ciphers, which often involve applying a sequence of
/// transformations to data.
///
/// Implementors of this trait should provide methods for applying a specific linear
/// transformation and its inverse to the implementing data structure.
pub trait LinearTransformation {
    /// Apply the linear transformation.
    ///
    /// This method applies a linear transformation to the implementing data structure.
    /// Linear transformations are commonly used in cryptographic operations, such as block
    /// ciphers, which often involve applying a sequence of transformations to data.
    fn apply_linear_transformation(&mut self);

    /// Apply the inverted linear transformation.
    ///
    /// This method applies the inverse of a linear transformation to the implementing
    /// data structure. Inverting linear transformations is a crucial step in cryptographic
    /// operations, such as block ciphers, where the transformations applied during encryption
    /// need to be reversed during decryption.
    fn apply_inverted_linear_transformation(&mut self);
}
