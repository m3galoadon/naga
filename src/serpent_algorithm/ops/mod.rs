// NAGA - megaloadon - 2023

//! This module contains structures, types, and constants related to the regular operations used in the Serpent cipher algorithm.
//!
//! Serpent is a symmetric block cipher with a 128-bit block size and a variable key size. This module provides definitions and implementations for the various operations used in the Serpent cipher, such as linear transformations, permutations, S-boxes, and XOR tables.
//!
//! # Submodules
//! * `linear_transform`: Contains the implementation of linear transformations used in the Serpent algorithm.
//! * `permutation`: Contains the implementation of permutation operations used in the Serpent algorithm.
//! * `sbox`: Contains the implementation of S-boxes, which are non-linear substitution tables used in the Serpent algorithm.
//! * `xor_table`: Contains the implementation of XOR tables, used for bitwise XOR operations in the Serpent algorithm.

pub mod linear_transform;
pub mod permutation;
pub mod sbox;
pub mod xor_table;
