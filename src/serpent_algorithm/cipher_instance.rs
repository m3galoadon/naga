// NAGA - megaloadon - 2023

use super::{
    constants::{BITS_PER_WORD, IV_LEN, SALT_LEN, WORDS_PER_BLOCK},
    data_types::{Block, InitializationVector, KeyInstance},
    utils::{generate_salt, FromBlocks, GetBit, SetBit, ToBlocks},
};
use rayon::prelude::*;
use zeroize::Zeroize;

/// Represents the supported cipher modes for encryption and decryption.
///
/// Available cipher modes are:
/// * `ECB`: Electronic Codebook mode.
/// * `CBC`: Cipher Block Chaining mode, requires an `InitializationVector`.
/// * `CFB1`: Cipher Feedback mode with a feedback length of 1 bit, requires an `InitializationVector`.
#[derive(Debug, Zeroize)]
pub enum CipherMode {
    ECB,
    CBC,
    CFB1,
    CTR,
}

/// Provides a custom implementation of the `Drop` trait for the `CipherMode` enum.
impl Drop for CipherMode {
    fn drop(&mut self) {
        self.zeroize();
    }
}

/// Represents a cipher instance with its mode and associated key material.
#[derive(Debug, Zeroize)]
pub struct CipherInstance {
    mode: CipherMode,
    password: Vec<u8>,
}

impl CipherInstance {
    /// Constructs a new `CipherInstance` with the specified `CipherMode` and password.
    ///
    /// # Arguments
    ///
    /// * `mode` - The `CipherMode` to use for the `CipherInstance`. It determines the encryption or decryption mode.
    /// * `password` - A `Vec<u8>` representing the password to be used for encryption or decryption.
    ///
    /// # Returns
    ///
    /// A new `CipherInstance` with the specified `mode` and `password`.
    #[allow(unused_assignments)]
    pub fn new(mode: CipherMode, password: &[u8]) -> Self {
        Self {
            mode,
            password: password.to_vec(),
        }
    }

    // TODO : DOC
    pub fn encrypt(&mut self, data: &mut Vec<u8>) -> Result<(), &'static str> {
        // Generate all through here
        let mut salt = generate_salt(SALT_LEN);
        let initial_iv: InitializationVector = Block::initialization_vector();
        let mut iv = initial_iv.clone();
        let key_instance = match KeyInstance::new_from_salt(&self.password, &salt) {
            Ok(ki) => ki,
            Err(e) => {
                salt.zeroize();
                return Err(e);
            }
        };

        // data to blocks
        let mut blocks = data.to_blocks(true);

        match self.mode {
            CipherMode::ECB => blocks
                .par_iter_mut()
                .for_each(|block| block.encrypt(&key_instance.sub_keys)),
            CipherMode::CBC => blocks.iter_mut().for_each(|block| {
                (0..WORDS_PER_BLOCK).for_each(|i| block[i] = block[i] ^ iv[i]);
                block.encrypt(&key_instance.sub_keys);
                iv = block.clone();
            }),
            CipherMode::CFB1 => blocks.iter_mut().for_each(|block| {
                let mut tmp = iv.clone();
                for i in 0..(BITS_PER_WORD * WORDS_PER_BLOCK) {
                    tmp.encrypt(&key_instance.sub_keys);
                    let mut cipher_text_bit =
                        tmp.get_bit((BITS_PER_WORD * WORDS_PER_BLOCK) - 1) ^ block.get_bit(i);
                    block.set_bit(i, cipher_text_bit);
                    iv.shift_block_left(cipher_text_bit);

                    // zeroize temporary values
                    cipher_text_bit.zeroize();
                }
                tmp.zeroize();
            }),
            CipherMode::CTR => {
                let mut plain_blocks = blocks.clone();
                // For blocks
                plain_blocks
                    .par_iter_mut()
                    .enumerate()
                    .for_each(|(block_index, plain_block)| {
                        // TODO PAR
                        // create counter
                        let mut counter_block = Block {
                            words: [iv[0], iv[1], iv[2], block_index as u32],
                        };
                        // encrypt
                        counter_block.encrypt(&key_instance.sub_keys);

                        // xor clear block and counter
                        let block = counter_block ^ plain_block.clone();

                        // Add this block to the result
                        *plain_block = block;
                    });

                // End encryption
                blocks = plain_blocks;
            }
        }

        // Blocks to data
        let encrypted_data = Vec::<u8>::from_blocks(blocks, false);

        // Create result
        match self.mode {
            // Store IV
            CipherMode::ECB => (),
            _ => salt.extend(Vec::<u8>::from_blocks(vec![initial_iv], false)),
        }

        salt.extend(encrypted_data); // Store data (extend from salt to avoid using more vectors)

        Ok(*data = salt)
    }

    // TODO DOC
    pub fn decrypt(&mut self, data: &mut Vec<u8>) -> Result<(), &'static str> {
        // Get salt, keys and IV
        let mut salt: Vec<u8> = data.drain(0..SALT_LEN).collect(); // Drain salt
        let key_instance: KeyInstance = match KeyInstance::new_from_salt(&self.password, &salt) {
            Ok(ki) => ki,
            Err(e) => {
                salt.zeroize();
                data.zeroize(); // Data is destroyed in case decryption has failed
                return Err(e);
            }
        };
        let mut iv_block = Block::default();

        match self.mode {
            CipherMode::ECB => (),
            _ => iv_block = data.drain(0..IV_LEN).collect::<Vec<u8>>().to_blocks(false)[0].clone(),
        }

        let mut blocks: Vec<Block> = data.to_blocks(false);

        match &self.mode {
            CipherMode::ECB => blocks
                .par_iter_mut()
                .for_each(|block| block.decrypt(&key_instance.sub_keys)),
            CipherMode::CBC => blocks.iter_mut().for_each(|block| {
                let mut tmp = Block::new(block.words);
                tmp.decrypt(&key_instance.sub_keys);
                tmp = tmp.clone() ^ iv_block.clone();
                iv_block = block.clone();
                *block = tmp;
            }),
            CipherMode::CFB1 => blocks.iter_mut().for_each(|block| {
                let mut tmp = iv_block.clone();
                for i in 0..(BITS_PER_WORD * WORDS_PER_BLOCK) {
                    tmp.encrypt(&key_instance.sub_keys);
                    let mut cipher_text_bit = block.get_bit(i);
                    block.set_bit(
                        i,
                        tmp.get_bit((BITS_PER_WORD * WORDS_PER_BLOCK) - 1) ^ cipher_text_bit,
                    );
                    iv_block.shift_block_left(cipher_text_bit);

                    cipher_text_bit.zeroize();
                }
            }),
            CipherMode::CTR => {
                let mut plain_blocks = blocks.clone();
                // For blocks
                plain_blocks
                    .par_iter_mut()
                    .enumerate()
                    .for_each(|(block_index, plain_block)| {
                        // TODO PAR
                        // create counter
                        let mut counter_block = Block {
                            words: [iv_block[0], iv_block[1], iv_block[2], block_index as u32],
                        };
                        // encrypt
                        counter_block.encrypt(&key_instance.sub_keys);

                        // xor clear block and counter
                        let block = counter_block ^ plain_block.clone();

                        // Add this block to the result
                        *plain_block = block;
                    });

                // End encryption
                blocks = plain_blocks;
            }
        }

        // Blocks to data
        Ok(*data = Vec::<u8>::from_blocks(blocks, true))
    }
}

/// Implements the `Drop` trait for `CipherInstance`.
///
/// This implementation is used to zeroize the sensitive data held by the `CipherInstance` struct when it goes out of scope.
impl Drop for CipherInstance {
    fn drop(&mut self) {
        self.password.zeroize();
        self.mode.zeroize();
        self.zeroize();
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn encrypt_decrypt_ecb() {
        let data: Vec<u8> = b"This is some really random data you know".to_vec();
        let mut data_c: Vec<u8> = data.clone();
        let mut ci: CipherInstance = CipherInstance::new(CipherMode::ECB, b"user_password123$");
        ci.encrypt(&mut data_c).unwrap();

        assert_ne!(data, data_c, "Data is not encrypted!");

        ci.decrypt(&mut data_c).unwrap();

        assert_eq!(data, data_c, "Data is not decrypted!");
    }

    /// Tests the encryption and decryption process using the same `CipherInstance` with CBC mode.
    ///
    /// This test encrypts and decrypts a sample data with the same `CipherInstance` in CBC mode,
    /// ensuring that the encrypted data is different from the original data, and that
    /// the decrypted data is equal to the original data.
    #[test]
    fn encrypt_and_decrypt_with_same_instance_cbc() {
        let data: Vec<u8> = b"This is some really random data you know".to_vec();
        let mut data_c: Vec<u8> = data.clone();
        let mut ci: CipherInstance = CipherInstance::new(CipherMode::CBC, b"user_password123$");
        ci.encrypt(&mut data_c).unwrap();

        assert_ne!(data, data_c, "Data is not encrypted!");

        ci.decrypt(&mut data_c).unwrap();

        assert_eq!(data, data_c, "Data is not decrypted!");
    }

    /// Tests the encryption and decryption process using the same `CipherInstance` with CTR mode.
    ///
    /// This test encrypts and decrypts a sample data with the same `CipherInstance` in CTR mode,
    /// ensuring that the encrypted data is different from the original data, and that
    /// the decrypted data is equal to the original data.
    #[test]
    fn encrypt_and_decrypt_with_same_instance_ctr() {
        let data: Vec<u8> = b"This is some really random data you know".to_vec();
        let mut data_c: Vec<u8> = data.clone();
        let mut ci: CipherInstance = CipherInstance::new(CipherMode::CTR, b"user_password123$");
        ci.encrypt(&mut data_c).unwrap();

        assert_ne!(data, data_c, "Data is not encrypted!");

        ci.decrypt(&mut data_c).unwrap();

        assert_eq!(data, data_c, "Data is not decrypted!");
    }

    /// Tests the encryption and decryption process using two different `CipherInstance`s with CBC mode.
    ///
    /// This test encrypts a sample data with one `CipherInstance` in CBC mode, and then decrypts
    /// the encrypted data with another `CipherInstance` using the same mode and password.
    /// It ensures that the encrypted data is different from the original data, and that
    /// the decrypted data is equal to the original data.
    #[test]
    fn encrypt_and_decrypt_with_different_instance_cbc() {
        let data: Vec<u8> = b"This is some really random data you know".to_vec();
        let mut data_c: Vec<u8> = data.clone();
        let mut ci_input: CipherInstance =
            CipherInstance::new(CipherMode::CBC, b"user_password123$");
        ci_input.encrypt(&mut data_c).unwrap();

        assert_ne!(data, data_c, "Data is not encrypted!");

        let mut ci_output: CipherInstance =
            CipherInstance::new(CipherMode::CBC, b"user_password123$");

        ci_output.decrypt(&mut data_c).unwrap();

        assert_eq!(data, data_c, "Data is not decrypted!");
    }

    /// Tests the encryption and decryption process using two different `CipherInstance`s with CTR mode.
    ///
    /// This test encrypts a sample data with one `CipherInstance` in CTR mode, and then decrypts
    /// the encrypted data with another `CipherInstance` using the same mode and password.
    /// It ensures that the encrypted data is different from the original data, and that
    /// the decrypted data is equal to the original data.
    #[test]
    fn encrypt_and_decrypt_with_different_instance_ctr() {
        let data: Vec<u8> = b"This is some really random data you know".to_vec();
        let mut data_c: Vec<u8> = data.clone();
        let mut ci_input: CipherInstance =
            CipherInstance::new(CipherMode::CTR, b"user_password123$");
        ci_input.encrypt(&mut data_c).unwrap();

        assert_ne!(data, data_c, "Data is not encrypted!");

        let mut ci_output: CipherInstance =
            CipherInstance::new(CipherMode::CTR, b"user_password123$");

        ci_output.decrypt(&mut data_c).unwrap();

        assert_eq!(data, data_c, "Data is not decrypted!");
    }

    /// Tests the encryption and decryption process using two different `CipherInstance`s with ECB mode and wrong password.
    ///
    /// This test encrypts a sample data with one `CipherInstance` in ECB mode, and then attempts to decrypt
    /// the encrypted data with another `CipherInstance` using the same mode but a wrong password.
    /// It ensures that the encrypted data is different from the original data, and that
    /// the decryption fails, resulting in an output that is different from the original data.
    #[test]
    fn encrypt_and_decrypt_wrong_password_ecb() {
        let data: Vec<u8> = b"This is some really random data you know".to_vec();
        let mut data_c: Vec<u8> = data.clone();
        let mut ci_input: CipherInstance =
            CipherInstance::new(CipherMode::ECB, b"user_password123$");
        ci_input.encrypt(&mut data_c).unwrap();

        assert_ne!(data, data_c, "Data is not encrypted!");

        let mut ci_output: CipherInstance = CipherInstance::new(CipherMode::ECB, b"adminadmin");

        ci_output.decrypt(&mut data_c).unwrap();

        assert_ne!(data, data_c, "Data is decrypted!");
    }

    /// Tests the encryption and decryption process using two different `CipherInstance`s with CBC mode and wrong password.
    ///
    /// This test encrypts a sample data with one `CipherInstance` in CBC mode, and then attempts to decrypt
    /// the encrypted data with another `CipherInstance` using the same mode but a wrong password.
    /// It ensures that the encrypted data is different from the original data, and that
    /// the decryption fails, resulting in an output that is different from the original data.
    #[test]
    fn encrypt_and_decrypt_wrong_password_cbc() {
        let data: Vec<u8> = b"This is some really random data you know".to_vec();
        let mut data_c: Vec<u8> = data.clone();
        let mut ci_input: CipherInstance =
            CipherInstance::new(CipherMode::CBC, b"user_password123$");
        ci_input.encrypt(&mut data_c).unwrap();

        assert_ne!(data, data_c, "Data is not encrypted!");

        let mut ci_output: CipherInstance = CipherInstance::new(CipherMode::CBC, b"adminadmin");

        ci_output.decrypt(&mut data_c).unwrap();

        assert_ne!(data, data_c, "Data is decrypted!");
    }

    /// Tests the encryption and decryption process using two different `CipherInstance`s with CFB1 mode and wrong password.
    ///
    /// This test encrypts a sample data with one `CipherInstance` in CFB1 mode, and then attempts to decrypt
    /// the encrypted data with another `CipherInstance` using the same mode but a wrong password.
    /// It ensures that the encrypted data is different from the original data, and that
    /// the decryption fails, resulting in an output that is different from the original data.
    #[test]
    fn encrypt_and_decrypt_wrong_password_cfb1() {
        let data: Vec<u8> = b"This is some really random data you know".to_vec();
        let mut data_c: Vec<u8> = data.clone();
        let mut ci_input: CipherInstance =
            CipherInstance::new(CipherMode::CFB1, b"user_password123$");
        ci_input.encrypt(&mut data_c).unwrap();

        assert_ne!(data, data_c, "Data is not encrypted!");

        let mut ci_output = CipherInstance::new(CipherMode::CFB1, b"adminadmin");

        ci_output.decrypt(&mut data_c).unwrap();

        assert_ne!(data, data_c, "Data is decrypted!");
    }

    /// Tests the encryption and decryption process using two different `CipherInstance`s with CTR mode and wrong password.
    ///
    /// This test encrypts a sample data with one `CipherInstance` in CTR mode, and then attempts to decrypt
    /// the encrypted data with another `CipherInstance` using the same mode but a wrong password.
    /// It ensures that the encrypted data is different from the original data, and that
    /// the decryption fails, resulting in an output that is different from the original data.
    #[test]
    fn encrypt_and_decrypt_wrong_password_ctr() {
        let data: Vec<u8> = b"This is some really random data you know".to_vec();
        let mut data_c: Vec<u8> = data.clone();
        let mut ci_input: CipherInstance =
            CipherInstance::new(CipherMode::CTR, b"user_password123$");
        ci_input.encrypt(&mut data_c).unwrap();

        assert_ne!(data, data_c, "Data is not encrypted!");

        let mut ci_output = CipherInstance::new(CipherMode::CTR, b"adminadmin");

        ci_output.decrypt(&mut data_c).unwrap();

        assert_ne!(data, data_c, "Data is decrypted!");
    }

    // TODO :
    // + Stress
    // + Wrong salt (inject)
    // + Wrong IV (inject)
}
