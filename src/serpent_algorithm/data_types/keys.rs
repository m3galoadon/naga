// NAGA - megaloadon - 2023

use super::{
    super::{
        constants::{BITS_PER_WORD, PHI, ROUNDS, SALT_LEN},
        ops::{
            permutation::{Initial, Permutation, PermutationTable, PermutationTableType},
            sbox::Sbox,
        },
        utils::{GetBit, SetBit},
    },
    Bit, Nibble, Word,
};
use sha3::{Digest, Sha3_256};
use zeroize::Zeroize;

/// `Key` type
///
/// A `Key` is an array of 8 `Word`s, typically used for representing encryption or decryption keys.
pub type Key = [Word; 8];

/// `SubKey` type
///
/// A `SubKey` is an array of 4 `Word`s, used for representing intermediate keys
/// during key schedule operations.
pub type SubKey = [Word; 4];

/// `SubKeys` type
///
/// `SubKeys` is an array of 33 `SubKey`s, used for storing all subkeys generated
/// during key schedule operations.
pub type SubKeys = [SubKey; 33];

/// Implementation of the `GetBit` trait for `SubKey`.
///
/// Provides the ability to retrieve a single bit from a `SubKey` at a specified position.
impl GetBit for SubKey {
    /// Retrieves the bit at the specified `position` within the `SubKey`.
    ///
    /// # Arguments
    ///
    /// * `position` - The position/index of the bit to be retrieved from the `SubKey`.
    ///                The value should be in the range of 0 to 127 (inclusive).
    ///
    /// # Returns
    ///
    /// * `Bit` - The bit (boolean value) found at the specified position within the `SubKey`.
    fn get_bit(&self, position: usize) -> Bit {
        self[position / BITS_PER_WORD].get_bit(position % BITS_PER_WORD)
    }
}

/// Implementation of the `SetBit` trait for `SubKey`.
///
/// Provides the ability to set a single bit within a `SubKey` at a specified position.
impl SetBit for SubKey {
    /// Sets the bit at the specified `position` within the `SubKey` to the provided `bit` value.
    ///
    /// # Arguments
    ///
    /// * `position` - The position/index of the bit to be set within the `SubKey`.
    ///                The value should be in the range of 0 to 127 (inclusive).
    /// * `bit`      - The `Bit` (boolean value) to be set at the specified position within the `SubKey`.
    fn set_bit(&mut self, position: usize, bit: Bit) {
        let word_index = position / BITS_PER_WORD;
        let bit_position = position % BITS_PER_WORD;
        self[word_index].set_bit(bit_position, bit);
    }
}

/// Implementation of the `Permutation` trait for `SubKey`.
///
/// Provides the ability to apply a permutation to a `SubKey`.
impl Permutation for SubKey {
    /// Applies the permutation defined by the `Table` type parameter to the `SubKey`.
    ///
    /// # Type Parameters
    ///
    /// * `Table` - A type implementing the `PermutationTableType` trait, which provides
    ///             the permutation table to be applied to the `SubKey`.
    fn apply_permutation<Table: PermutationTableType>(&mut self) {
        let table: PermutationTable = Table::get();
        let mut output = Self::default();

        (0..128).for_each(|position| {
            output.set_bit(position, self.get_bit(table[position] as usize));
        });

        *self = output;
    }
}

/// `KeyInstance` Struct
///
/// A structure for storing a user key, its corresponding subkeys, and optional salt.
///
/// # Attributes
///
/// * `user_key` - The main key used for encryption or decryption, represented as an array of 8 `Word`s (`Key` type).
/// * `sub_keys` - A set of 33 subkeys (`SubKeys` type) derived from the main key, typically used in key schedule operations.
/// * `salt`     - An optional `Vec<u8>` containing salt data, used for strengthening the key or enhancing security.
///
/// # Derives
///
/// * `Debug`   - Enables pretty-printing for debugging purposes.
/// * `Zeroize` - Ensures sensitive data is securely zeroed out when the `KeyInstance` is dropped.
#[derive(Debug, Zeroize, PartialEq)]
pub struct KeyInstance {
    user_key: Key,
    pub sub_keys: SubKeys,
}

impl KeyInstance {
    /// Create a new `KeyInstance` from the provided `key_material` and `salt`.
    ///
    /// This method derives a user key from the given `key_material` and `salt`, and then generates
    /// the subkeys using the derived user key. It returns a `Result` containing the created `KeyInstance`
    /// or an error message if the provided `salt` has an invalid length.
    ///
    /// # Arguments
    ///
    /// * `key_material`: A byte slice representing the key material to be used for deriving the user key.
    /// * `salt`: A byte slice representing the salt to be used for deriving the user key.
    ///
    /// # Errors
    ///
    /// Returns an error if the provided `salt` has an invalid length.
    #[allow(unused_assignments)]
    pub fn new_from_salt(mut key_material: &[u8], salt: &[u8]) -> Result<Self, &'static str> {
        if salt.len() != SALT_LEN {
            return Err("Invalid salt length!");
        }; // salt length error
        let user_key = Self::derive_key(&key_material, &salt);
        key_material = &[]; // Don't leak addr

        // Generate subkeys using user_key
        let sub_keys: SubKeys = Self::derive_subkeys(&user_key);

        Ok(Self { user_key, sub_keys })
    }

    /// Derives a `Key` from the given `key_material` and `salt`.
    ///
    /// The function creates a SHA3-256 hash of the combined `key_material` and `salt`
    /// and converts the resulting hash into a `Key`.
    ///
    /// # Arguments
    ///
    /// * `key_material` - A byte slice (`&[u8]`) containing the key material to be used for
    ///                    generating the user key.
    /// * `salt`         - A byte slice (`&[u8]`) containing the salt to be used in key derivation.
    ///
    /// # Returns
    ///
    /// * `Key` - A derived `Key` created from the `key_material` and `salt`.
    #[allow(unused_assignments)]
    fn derive_key(mut key_material: &[u8], mut salt: &[u8]) -> Key {
        let mut hasher = Sha3_256::new();
        hasher.update(&key_material);
        hasher.update(&salt);
        let mut hash_result = hasher.finalize();

        // Don't leak addr
        key_material = &[];
        salt = &[];

        let mut user_key: Key = [0u32; 8];
        for i in 0..4 {
            let offset = i * 4;
            user_key[i] = u32::from_be_bytes([
                hash_result[offset],
                hash_result[offset + 1],
                hash_result[offset + 2],
                hash_result[offset + 3],
            ]);
        }

        // Don't leak Hash
        hash_result.zeroize();

        user_key
    }

    /// Derives `SubKeys` from the given `user_key`.
    ///
    /// This function implements the key schedule algorithm to derive subkeys
    /// from the user key, performing affine rotation and applying S-boxes and
    /// initial permutation.
    ///
    /// # Arguments
    ///
    /// * `user_key` - A reference to a `Key` from which the subkeys will be derived.
    ///
    /// # Returns
    ///
    /// * `SubKeys` - An array of derived `SubKey`s.
    #[allow(unused_assignments)]
    fn derive_subkeys(mut user_key: &Key) -> SubKeys {
        let mut w: [Word; 140] = [0; 140];

        // First 8 bytes from user_key
        for i in 0..8 {
            w[i] = user_key[i];
        }

        // Don't leak addr
        user_key = &[0; 8];

        for i in 8..140 {
            let val = w[i - 8] ^ w[i - 5] ^ w[i - 3] ^ w[i - 1] ^ PHI ^ ((i - 8) as u32);
            w[i] = val.rotate_left(11);
        }

        let mut subkeys: SubKeys = [[0; 4]; 33];
        let mut k: [Word; 132] = [0; 132];

        // Calculate key values
        for i in 0..(ROUNDS + 1) {
            let which_sbox: usize = (ROUNDS + 3 - i) % ROUNDS;
            for j in 0..32 {
                let input: Nibble = [
                    w[(0 + 4 * i) + 8].get_bit(j),
                    w[(1 + 4 * i) + 8].get_bit(j),
                    w[(2 + 4 * i) + 8].get_bit(j),
                    w[(3 + 4 * i) + 8].get_bit(j),
                ];
                let output = input.apply_sbox(which_sbox);
                for l in 0..4 {
                    k[l + 4 * i] |= (output.get_bit(l) as Word) << j;
                }
            }
        }

        // w not needed anymore
        w.zeroize();

        // Assign those values to subkeys
        for i in 0..33 {
            for j in 0..4 {
                subkeys[i][j] = k[4 * i + j];
            }
        }

        // k not needed anymore
        k.zeroize();

        // Initial Permutation
        for i in 0..33 {
            subkeys[i].apply_permutation::<Initial>();
        }

        subkeys
    }
}

/// Provides a custom implementation for the `Drop` trait on the `KeyInstance` struct.
///
/// When a `KeyInstance` goes out of scope and is dropped, this implementation
/// ensures that all sensitive data contained within the instance is securely
/// zeroized to prevent information leaks.
impl Drop for KeyInstance {
    fn drop(&mut self) {
        self.user_key.zeroize();
        self.sub_keys.iter_mut().for_each(|v| v.zeroize());
        self.sub_keys.zeroize();
        self.zeroize();
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use rand::{thread_rng, Rng};
    use std::iter::repeat_with;

    /// Test to verify that the subkey generation matches the expected subkeys based on a C reference implementation.
    ///
    /// This test ensures that the subkey generation in this implementation matches the results from a C reference implementation.
    #[test]
    fn test_subkey_gen() {
        // Result based on C reference implementation
        let user_key: Key = [
            0x00010203, 0x04050607, 0x08090a0b, 0x0c0d0e0f, 0x10111213, 0x14151617, 0x18191a1b,
            0x1c1d1e1f,
        ];
        let expected_subkeys: SubKeys = [
            [0x14072569, 0x9136ffda, 0x43f6395e, 0x4c68eb91],
            [0x31f4719, 0xe0efbb7f, 0xd742a8ec, 0xd006408a],
            [0x5b15a7f6, 0x249d424d, 0x501ed9e1, 0xe29bc3f],
            [0x37e0fa34, 0x61fa0ab9, 0xd87d02f, 0xd8b427a4],
            [0x64f1dc7, 0x63077ba6, 0x49536bd4, 0x431949e1],
            [0x915b50b6, 0x501bc85, 0x17336c20, 0x3d13df0d],
            [0x1701b4c5, 0x335d3759, 0x757ee62f, 0xea101d77],
            [0x9ccee62e, 0xebeb3eed, 0x6f4c065f, 0xacbfaa6f],
            [0xa72f303a, 0x631dfb79, 0x72658633, 0xb7ba4151],
            [0x11d75eb1, 0xc70e0a61, 0xdac49f93, 0x20634680],
            [0xe7034dea, 0xa822bc46, 0x28a10c06, 0x253de176],
            [0x48bf359c, 0x1b6753a1, 0xdda8516b, 0x4ba5f5d6],
            [0xe64b0389, 0x96d6f306, 0xe90c09bf, 0x1edea756],
            [0x93743c0a, 0x3173edbe, 0xed4488f7, 0xd9f93a8e],
            [0x4eff86ee, 0x1f8a905b, 0x74b4e4c0, 0x46fd695f],
            [0x28af3eee, 0xd1812c5c, 0xa51d5e27, 0xb732b8c5],
            [0xf0cb4132, 0x1a4a3b54, 0xb232dcf9, 0x3a535a35],
            [0x22350478, 0xfae11a54, 0xe6b5f74f, 0xb4be5204],
            [0x616baf47, 0xd7e1f085, 0x44883ab, 0xfe2eda7c],
            [0x727d7bc2, 0xe1371eb, 0xfc307547, 0x3027be94],
            [0xafa7ae54, 0x648250ec, 0xc99b1010, 0x58d72b0d],
            [0xb304912d, 0x16c746d, 0x8e6076c6, 0xe3c79c4e],
            [0x2da9760c, 0x6595434e, 0x905b1d01, 0x798ea463],
            [0x65537e8d, 0xc7a932a1, 0x6a888ab3, 0x892cda42],
            [0x594e189, 0xa45deee, 0x7a40b010, 0x1e0866a9],
            [0x3a7cfa4c, 0x5df6df4, 0x59631863, 0xcbc3309f],
            [0xfc6b9fed, 0xa8abebde, 0xafcca001, 0x47da0b55],
            [0x91d1fa23, 0x1d8e0e0f, 0xc79e8b6b, 0xdc8ae9e8],
            [0xcdd11bd2, 0x65968e39, 0xea804d01, 0x8639a25f],
            [0xa831679c, 0xb26aa2ce, 0xcb94cd6e, 0x5cc2e3b3],
            [0x742631ca, 0x2a5d4368, 0xb0d832d0, 0xeca8ecfc],
            [0xc982a64c, 0xbdfe62ec, 0xe9be985e, 0x4e39f71f],
            [0xcd03fc2e, 0xe4ea006, 0x157f874d, 0xb12447cb],
        ];

        assert_eq!(
            expected_subkeys,
            KeyInstance::derive_subkeys(&user_key),
            "Generated SubKeys does not match!"
        )
    }

    /// Stress test to ensure the correct sizes for the KeyInstance members when generating new instances with random key material.
    ///
    /// This test ensures that the KeyInstance creation process consistently generates instances with the correct sizes for its members.
    #[test]
    fn key_instance_new_stress_test() {
        use super::super::super::utils::generate_salt;
        let mut rng = thread_rng();

        for _ in 0..1000 {
            // Generate random key material with a length between 1 and 64 bytes
            let key_material_len: usize = rng.gen_range(1..=64);
            let key_material: Vec<u8> = repeat_with(|| rng.gen::<u8>())
                .take(key_material_len)
                .collect();

            // Create a new KeyInstance using the generated key_material
            let key_instance =
                KeyInstance::new_from_salt(&key_material, &generate_salt(SALT_LEN)).unwrap();

            // Check that the members of the KeyInstance have the correct sizes
            assert_eq!(key_instance.user_key.len(), 8, "User key size is incorrect");
            assert_eq!(
                key_instance.sub_keys.len(),
                33,
                "SubKeys array size is incorrect"
            );
            for sub_key in &key_instance.sub_keys {
                assert_eq!(sub_key.len(), 4, "SubKey size is incorrect");
            }
        }
    }

    /// Test to ensure that the `derive_key` method produces the same output for identical inputs.
    ///
    /// This test ensures that the `derive_key` method consistently generates the same output for identical inputs.
    #[test]
    fn derive_key_same_input() {
        let user_key: &[u8] = b"hello world!";
        let salt: &[u8] = b"01234567890123456789012345678901";

        let r1 = KeyInstance::derive_key(user_key, salt);
        let r2 = KeyInstance::derive_key(user_key, salt);

        assert_eq!(r1, r2, "Both results should be the same!");
    }

    /// Test to ensure that the `derive_key` method produces different outputs for different inputs.
    ///
    /// This test ensures that the `derive_key` method generates different output when provided with different inputs.
    #[test]
    fn derive_key_diff_input() {
        let user_key: &[u8] = b"hello world!";
        let user_key2: &[u8] = b"Good bye mars!";
        let salt: &[u8] = b"01234567890123456789012345678901";
        let salt2: &[u8] = b"98765432109876543210987654321098";

        let r1 = KeyInstance::derive_key(user_key, salt);
        let r2 = KeyInstance::derive_key(user_key, salt);
        let r3 = KeyInstance::derive_key(user_key2, salt);
        let r4 = KeyInstance::derive_key(user_key, salt2);

        assert_eq!(r1, r2, "R1 & R2 should be the same!");
        assert_ne!(
            r1, r3,
            "R1 & R3 should not be the same! (different user_key)"
        );
        assert_ne!(r1, r4, "R1 & R3 should not be the same! (different salt)");
    }

    /// Test to ensure that the `new_from_salt` method produces the same output for the same input.
    ///
    /// This test ensures that the `new_from_salt` method generates the same output when provided with the same input.
    #[test]
    fn key_from_salt_same_input() {
        let user_key: &[u8] = b"hello world!";
        let salt: &[u8] = b"01234567890123456789012345678901";

        let r1 = KeyInstance::new_from_salt(user_key, salt).unwrap();
        let r2 = KeyInstance::new_from_salt(user_key, salt).unwrap();

        assert_eq!(r1, r2, "Both results should be the same!");
    }

    /// Test to ensure that the `new_from_salt` method produces different outputs for different inputs.
    ///
    /// This test ensures that the `new_from_salt` method generates different outputs when provided with different inputs.
    #[test]
    fn key_from_salt_diff_input() {
        let user_key: &[u8] = b"hello world!";
        let user_key2: &[u8] = b"Good bye mars!";
        let salt: &[u8] = b"01234567890123456789012345678901";
        let salt2: &[u8] = b"98765432109876543210987654321098";

        let r1 = KeyInstance::new_from_salt(user_key, salt);
        let r2 = KeyInstance::new_from_salt(user_key, salt);
        let r3 = KeyInstance::new_from_salt(user_key2, salt);
        let r4 = KeyInstance::new_from_salt(user_key, salt2);

        assert_eq!(r1, r2, "R1 & R2 should be the same!");
        assert_ne!(
            r1, r3,
            "R1 & R3 should not be the same! (different user_key)"
        );
        assert_ne!(r1, r4, "R1 & R3 should not be the same! (different salt)");
    }
}
