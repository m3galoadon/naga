// NAGA - megaloadon - 2023

use crate::serpent_algorithm::{
    constants::LAST_NIBBLE_BIT,
    data_types::Bit,
    ops::sbox::{Sbox, INVERTED_SBOX, SBOX},
    utils::GetBit,
};
use zeroize::Zeroize;

/// A `Nibble` represents a 4-bit data unit, which is half the size of a byte.
///
/// A 'Nibble' is represented as an array of 4 `Bit`s, where a `Bit` is a boolean value
/// (`true` for 1, `false` for 0).
pub type Nibble = [Bit; 4];

/// Implementation of the `NibbleConversion` trait for `Nibble`.
///
/// Provides methods for converting `Nibble` to and from numeric types.
impl NibbleConversion for Nibble {
    /// Converts the `Nibble` into a `usize` value.
    ///
    /// # Returns
    ///
    /// * `usize` - The numeric value equivalent of the `Nibble`.
    fn to_usize(&self) -> usize {
        let mut result: usize = 0;

        for (index, bit) in self.iter().enumerate() {
            if *bit {
                result += 1 << index;
            }
        }

        result
    }

    /// Creates a `Nibble` from a numeric value.
    ///
    /// # Arguments
    ///
    /// * `val` - The numeric value to be converted into a `Nibble`. The value should be of type `u128`
    ///           or a type that can be converted into `u128`. The value will be reduced modulo 16 to
    ///           ensure it fits within a `Nibble`.
    ///
    /// # Returns
    ///
    /// * `Self` - A new `Nibble` representing the given numeric value.
    fn from_u<N: Into<u128> + Zeroize>(val: N) -> Self {
        let mut value = val.into() % 16;
        let mut nibble = [false; 4];

        for i in 0..4 {
            nibble[i] = (value >> i) & 1 == 1;
        }

        value.zeroize();

        nibble
    }
}

/// Implementation of the `Sbox` trait for `Nibble`.
///
/// Provides methods for applying substitution boxes (S-boxes) and their inverse
/// transformations to a `Nibble`.
impl Sbox for Nibble {
    /// Applies the specified S-box to the `Nibble`.
    ///
    /// # Arguments
    ///
    /// * `sbox` - The index of the S-box to be applied.
    ///
    /// # Returns
    ///
    /// * `Self` - A new `Nibble` resulting from applying the S-box transformation.
    fn apply_sbox(&self, sbox: usize) -> Self {
        Nibble::from_u(SBOX[sbox][self.to_usize()])
    }

    /// Applies the inverse of the specified S-box to the `Nibble`.
    ///
    /// # Arguments
    ///
    /// * `sbox` - The index of the S-box for which the inverse should be applied.
    ///
    /// # Returns
    ///
    /// * `Self` - A new `Nibble` resulting from applying the inverse S-box transformation.
    fn apply_sbox_inverse(&self, sbox: usize) -> Self {
        Nibble::from_u(INVERTED_SBOX[sbox][self.to_usize()])
    }
}

/// Implementation of the `GetBit` trait for `Nibble`.
///
/// Provides the ability to retrieve a single bit from a `Nibble` at a specified position.
impl GetBit for Nibble {
    /// Retrieves the bit at the specified `position` within the `Nibble`.
    ///
    /// # Arguments
    ///
    /// * `position` - The position/index of the bit to be retrieved from the `Nibble`. The value will be clamped
    ///                to the last valid index if it exceeds the maximum valid index for a `Nibble` (3).
    ///
    /// # Returns
    ///
    /// * `Bit` - The bit (boolean value) found at the specified position within the `Nibble`.
    fn get_bit(&self, position: usize) -> Bit {
        self[position.min(LAST_NIBBLE_BIT)]
    }
}

/// GetNibble Trait
///
/// The `GetNibble` trait provides a method to retrieve a nibble (a sequence of 4 bits)
/// from a data structure at a specified position.
///
/// A nibble is represented as an array of 4 `Bit`s, where a `Bit` is a boolean value
/// (`true` for 1, `false` for 0).
pub trait GetNibble {
    /// Retrieves the nibble at the specified `position`.
    ///
    /// # Arguments
    ///
    /// * `position` - The position/index in the data structure from which the nibble should be retrieved.
    ///
    /// # Returns
    ///
    /// * `Nibble` - The nibble found at the specified position.
    fn get_nibble(&self, position: usize) -> Nibble;
}

/// The `NibbleConversion` trait provides methods for converting between `Nibble` and integer types.
///
/// This trait defines methods for converting a `Nibble` to a `usize` and for creating a `Nibble` from
/// a numeric value that implements `Into<u128>` and `Zeroize`. This allows for more convenient
/// interactions between `Nibble` and other numeric types. The integer must implement 'Zeroize' to
/// ensure zeroization of sensible data into Nibbles.
pub trait NibbleConversion {
    /// Converts the `Nibble` to a `usize` value.
    ///
    /// This method returns the integer representation of the `Nibble`.
    fn to_usize(&self) -> usize;

    /// Creates a `Nibble` from a numeric value of type `N` that implements `Into<u128>` and `Zeroize`.
    ///
    /// The value will be converted into a `u128` and then into a `Nibble`. The original value will be
    /// zeroized for security purposes.
    fn from_u<N: Into<u128> + Zeroize>(value: N) -> Self;
}

#[cfg(test)]
mod test {
    use super::*;
    use rand::Rng;

    /// Test to ensure that the `Nibble` struct correctly handles the conversion between `u8` and `usize` types.
    ///
    /// This test ensures that the `Nibble` struct properly handles type conversions between `u8` and `usize`.
    #[test]
    fn test_nibbles() {
        assert_eq!(
            Nibble::from_u(1u8).to_usize(),
            1usize,
            "Nibble conversion failed!"
        )
    }

    /// Stress test to ensure that the `Nibble` struct correctly handles conversions between `u32`, `u8`, and `usize` types.
    ///
    /// This test ensures that the `Nibble` struct can consistently and accurately perform type conversions between `u32`, `u8`, and `usize`.
    #[test]
    fn nibble_conversion_stress_test() {
        let mut rng = rand::thread_rng();
        const ITERATIONS: usize = 10000;

        for _ in 0..ITERATIONS {
            // Generate a random u8 value
            let value: u32 = rng.gen_range(0..16);

            // Convert the value to a Nibble
            let nibble = Nibble::from_u(value);

            // Convert the Nibble back to a usize
            let converted_value = nibble.to_usize();

            // Assert that the original value and the converted value are equal
            assert_eq!(value as usize, converted_value, "Nibble conversion failed!");
        }
    }

    /// Test the correct application of the S-box to `Nibble`s.
    ///
    /// This test checks if the S-box is correctly applied to the `Nibble`s with the provided indexes.
    #[test]
    fn test_sbox() {
        let nibbles: [Nibble; 4] = [
            Nibble::from_u::<u8>(1),
            Nibble::from_u::<u8>(2),
            Nibble::from_u::<u8>(3),
            Nibble::from_u::<u8>(4),
        ];

        let expected_nibbles: [Nibble; 4] = [
            Nibble::from_u::<u8>(8),
            Nibble::from_u::<u8>(11),
            Nibble::from_u::<u8>(1),
            Nibble::from_u::<u8>(8),
        ];

        assert_eq!(
            expected_nibbles,
            [
                nibbles[0].apply_sbox(0),
                nibbles[1].apply_sbox(3),
                nibbles[2].apply_sbox(8),
                nibbles[3].apply_sbox(14)
            ],
            "Sboxed nibbles does not match!"
        )
    }
}
