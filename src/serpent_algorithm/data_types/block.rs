// NAGA - megaloadon - 2023

use super::{
    super::{
        constants::{
            BITS_PER_NIBBLE, BITS_PER_WORD, BLOCK_SIZE, MARKER, NIBBLES_PER_WORD, WORDS_PER_BLOCK,
        },
        ops::{
            linear_transform::LinearTransformation,
            permutation::{
                Final, FinalInverted, Initial, InitialInverted, Permutation, PermutationTable,
                PermutationTableType,
            },
            sbox::Sbox,
            xor_table::{XorTable, XorTableTransform, XOR_TABLE, XOR_TABLE_INVERSE},
        },
        utils::{GetBit, SetBit},
    },
    nibble::{GetNibble, NibbleConversion},
    Bit, InitializationVector, SubKeys, Word,
};
use rand::RngCore;
use std::ops::BitXor;
use std::ops::{Index, IndexMut};
use zeroize::Zeroize;

/// A representation of a data block used in cryptographic operations.
///
/// The `Block` struct consists of an array of `Word`s, where each `Word` is a 32-bit
/// unsigned integer (u32). The number of `Word`s per block is defined by the
/// constant `WORDS_PER_BLOCK`.
///
/// # Derives
///
/// * `Debug`       : Enables the display of `Block` struct for debugging purposes.
/// * `Zeroize`     : Allows securely zeroing the memory of the `Block` struct
///   when it is dropped to prevent information leaks.
/// * `PartialEq`   : Enables comparison between `Block` structs for equality.
/// * `Clone`       : Allows creating a deep copy of a `Block` struct.
#[derive(Debug, Zeroize, PartialEq, Clone)]
pub struct Block {
    pub words: [Word; WORDS_PER_BLOCK],
}

impl Block {
    /// Creates a new `Block` instance with the specified array of `Word`s.
    ///
    /// This constructor allows you to create a new `Block` by providing an
    /// array of `Word`s with the length defined by the `WORDS_PER_BLOCK` constant.
    ///
    /// # Arguments
    ///
    /// * `words`: An array of `Word`s (u32) with a length of `WORDS_PER_BLOCK`.
    pub fn new(words: [Word; WORDS_PER_BLOCK]) -> Self {
        Self { words }
    }

    /// Generates a random initialization vector for use in block ciphers.
    pub fn initialization_vector() -> InitializationVector {
        let mut iv = [0u8; 16];
        rand::thread_rng().fill_bytes(&mut iv);
        iv.into()
    }

    /// Shifts the bits of the block to the left and sets the least significant bit to the given input.
    ///
    /// # Arguments
    ///
    /// * `input` - The bit to set as the least significant bit of the block.
    pub fn shift_block_left(&mut self, mut input: Bit) {
        (1..WORDS_PER_BLOCK).for_each(|i| {
            self.words[i] <<= 1;
            let mut bit_to_set = self.words[i - 1].get_bit(BITS_PER_WORD - 1);
            self.words[i].set_bit(0, bit_to_set);
            bit_to_set.zeroize();
        });
        self.words[0] <<= 1;
        self.words[0].set_bit(0, input);
        input.zeroize();
    }

    /// Applies a substitution box (S-box) to each nibble of the block in parallel.
    ///
    /// # Arguments
    ///
    /// * `sbox_index` - The index of the S-box to apply.
    fn apply_parallel_sbox(&mut self, mut sbox_index: usize) {
        let mut new_block = Block::default(); // Init zero block
        sbox_index = sbox_index % 32;
        (0..WORDS_PER_BLOCK).for_each(|i| {
            new_block[i] = 0;
            (0..NIBBLES_PER_WORD).for_each(|j| {
                new_block[i] |= (self.words[i]
                    .get_nibble(j)
                    .apply_sbox(sbox_index)
                    .to_usize() as Word)
                    << (j * BITS_PER_NIBBLE)
                // }
            });
            // }
        });
        *self = new_block;
        sbox_index.zeroize();
    }

    /// Applies the inverse of a substitution box (S-box) to each nibble of the block in parallel.
    ///
    /// # Arguments
    ///
    /// * `sbox_index` - The index of the S-box to invert and apply.
    fn apply_inverted_parallel_sbox(&mut self, mut sbox_index: usize) {
        let mut new_block = Block::default(); // Init zero block
        sbox_index = sbox_index % 32;
        (0..WORDS_PER_BLOCK).for_each(|i| {
            new_block[i] = 0;
            (0..NIBBLES_PER_WORD).for_each(|j| {
                new_block[i] |= (self.words[i]
                    .get_nibble(j)
                    .apply_sbox_inverse(sbox_index)
                    .to_usize() as Word)
                    << (j * BITS_PER_NIBBLE)
            });
        });
        *self = new_block;
        sbox_index.zeroize();
    }

    /// Applies a single round of the Serpent cipher to the block using the given subkeys and key index.
    ///
    /// # Arguments
    ///
    /// * `keys` - The subkeys to use for the round.
    /// * `key_index` - The index of the subkeys to use for the round.
    fn apply_round(&mut self, mut keys: SubKeys, mut key_index: usize) {
        let mut output = Block::new(self.words) ^ Block::new(keys[key_index]);
        output.apply_parallel_sbox(key_index);
        if key_index <= 30 {
            output.apply_linear_transformation();
        } else if key_index == 31 {
            output = output ^ Block::new(keys[32]);
        }
        keys.zeroize();
        key_index.zeroize();
        *self = output;
    }

    /// Applies the inverse of a single round of the Serpent cipher to the block using the given subkeys and key index.
    ///
    /// # Arguments
    ///
    /// * `keys` - The subkeys to use for the round.
    /// * `key_index` - The index of the subkeys to use for the round.
    fn apply_inverted_round(&mut self, mut keys: SubKeys, mut key_index: usize) {
        let mut output = Block::new(self.words);
        if key_index <= 30 {
            output.apply_inverted_linear_transformation();
        } else if key_index == 31 {
            output = output ^ Block::new(keys[32]);
        }
        output.apply_inverted_parallel_sbox(key_index);
        output = output ^ Block::new(keys[key_index]);
        keys.zeroize();
        key_index.zeroize();
        *self = output;
    }

    /// Encrypts the data using the Serpent algorithm.
    ///
    /// This function applies the Serpent encryption process on the current `Block` instance, using the provided
    /// subkeys. It goes through the initial permutation, 32 rounds of encryption, and the final permutation.
    ///
    /// # Arguments
    ///
    /// * `keys` - A reference to a `SubKeys` instance containing the required subkeys for the encryption process.
    pub fn encrypt(&mut self, keys: &SubKeys) {
        self.apply_permutation::<Initial>(); // IP
        for i in 0..32 {
            // R[i]
            self.apply_round(*keys, i);
        }
        self.apply_permutation::<Final>(); // FP
    }

    /// Decrypts the data using the Serpent algorithm.
    ///
    /// This function applies the Serpent decryption process on the current `Block` instance, using the provided
    /// subkeys. It goes through the inverse final permutation, 32 rounds of decryption (in reverse order), and the
    /// inverse initial permutation.
    ///
    /// # Arguments
    ///
    /// * `keys` - A reference to a `SubKeys` instance containing the required subkeys for the decryption process.
    pub fn decrypt(&mut self, keys: &SubKeys) {
        self.apply_permutation::<FinalInverted>(); // FPI
        for i in (0..32).rev() {
            // R[i]
            self.apply_inverted_round(*keys, i);
        }
        self.apply_permutation::<InitialInverted>(); // IPI
    }
}

/// Implements the `BitXor` trait for the `Block` struct.
///
/// This trait allows bitwise exclusive or (XOR) operations to be performed on `Block` instances.
impl BitXor for Block {
    /// The output type of the `bitxor` operation, which is a `Block`.
    type Output = Self;

    /// Performs a bitwise exclusive or (XOR) operation on two `Block` instances.
    ///
    /// This method creates a new `Block` containing the result of the XOR operation
    /// between `self` and `rhs`.
    ///
    /// # Arguments
    ///
    /// * `rhs`: The right-hand side `Block` to perform the XOR operation with.
    ///
    /// # Returns
    ///
    /// A new `Block` containing the result of the XOR operation between `self` and `rhs`.
    fn bitxor(self, rhs: Self) -> Self::Output {
        let mut out = Block { words: [0; 4] };
        for i in 0..4 {
            out.words[i] = self.words[i] ^ rhs.words[i];
        }
        out
    }
}

/// Implements the `Permutation` trait for the `Block` struct.
///
/// This trait allows for applying permutation operations to the `Block` struct.
impl Permutation for Block {
    /// Applies a permutation operation to the `Block` using a permutation table.
    ///
    /// The permutation operation rearranges the bits of the `Block` based on the specified
    /// permutation table type.
    ///
    /// # Type Parameters
    ///
    /// * `Table`: The type of the permutation table to use. This type must implement the
    ///   `PermutationTableType` trait.
    fn apply_permutation<Table: PermutationTableType>(&mut self) {
        let table: PermutationTable = Table::get();
        let mut output = Self::default();

        for position in 0..BLOCK_SIZE {
            output.set_bit(position, self.get_bit(table[position] as usize));
        }

        *self = output;
    }
}

/// Implements the `GetBit` trait for the `Block` struct.
///
/// This trait allows for getting individual bits within the `Block` struct.
impl GetBit for Block {
    /// Returns the bit value (as a `bool`) at the specified position within the `Block`.
    ///
    /// # Parameters
    ///
    /// * `position`: The position of the bit to get within the `Block` (0-127).
    fn get_bit(&self, position: usize) -> Bit {
        self[position / BITS_PER_WORD].get_bit(position % BITS_PER_WORD)
    }
}

/// Implements the `SetBit` trait for the `Block` struct.
///
/// This trait allows for setting individual bits within the `Block` struct.
impl SetBit for Block {
    /// Sets the bit value (as a `bool`) at the specified position within the `Block`.
    ///
    /// # Parameters
    ///
    /// * `position`: The position of the bit to set within the `Block` (0-127).
    /// * `bit`: The value of the bit to set (`true` or `false`).
    fn set_bit(&mut self, position: usize, bit: Bit) {
        self[position / BITS_PER_WORD].set_bit(position % BITS_PER_WORD, bit);
    }
}

/// Implements the `Index<usize>` trait for the `Block` struct.
///
/// This trait allows for indexing the `Block` struct to access its underlying `Word` elements.
impl Index<usize> for Block {
    type Output = Word;

    /// Returns a reference to the `Word` at the specified index within the `Block` struct.
    fn index(&self, index: usize) -> &Self::Output {
        &self.words[index]
    }
}

/// Implements the `IndexMut<usize>` trait for the `Block` struct.
///
/// This trait allows for mutable indexing of the `Block` struct to access and modify its underlying `Word` elements.
impl IndexMut<usize> for Block {
    /// Returns a mutable reference to the `Word` at the specified index within the `Block` struct.
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.words[index]
    }
}

/// Implements the `From<[u8; 16]>` trait for the `Block` struct.
///
/// This trait allows for the conversion of a `[u8; 16]` byte array into a `Block` instance.
impl From<[u8; 16]> for Block {
    /// Converts a `[u8; 16]` byte array into a `Block` instance.
    ///
    /// The function converts the input `data` array into an array of `words` with a length of 4.
    /// Each word is constructed from a group of 4 bytes in the input array using little-endian order.
    /// The resulting `Block` instance contains the `words` array.
    ///
    /// The input array `data` and the temporary `offset` variable are zeroized for security reasons.
    fn from(mut data: [u8; 16]) -> Self {
        let mut words: [Word; 4] = [0u32; 4];
        for i in 0..4 {
            let mut offset = i * 4;
            words[i] = u32::from_le_bytes([
                data[offset],
                data[offset + 1],
                data[offset + 2],
                data[offset + 3],
            ]);
            offset.zeroize();
        }
        data.zeroize();
        Self { words }
    }
}

/// Implements the `Default` trait for the `Block` struct.
///
/// The `Default` trait provides a way to create an instance of `Block` with default values.
impl Default for Block {
    /// Returns a new `Block` instance with default values.
    ///
    /// The `words` field is initialized with an array of default `Word` values,
    /// where the length of the array is equal to `BLOCK_SIZE / BITS_PER_WORD`.
    fn default() -> Self {
        Self {
            words: [Word::default(); BLOCK_SIZE / BITS_PER_WORD],
        }
    }
}

/// Implements the `Drop` trait for the `Block` struct.
///
/// The `Drop` trait provides a method to perform actions when the `Block` instance
/// is being destroyed or goes out of scope.
impl Drop for Block {
    /// Zeroizes the `words` field and the `Block` instance itself when the `Block` is dropped.
    ///
    /// This method ensures that the memory is securely wiped when the `Block` is no longer needed,
    /// preventing potential data leaks.
    fn drop(&mut self) {
        // Zeroize le bloc de données lors de la destruction de la structure
        self.words.zeroize();
        self.zeroize();
    }
}

/// Implements the `XorTableTransform` trait for the `Block` struct.
///
/// The `XorTableTransform` trait provides methods to apply an XOR table
/// transformation on a `Block` instance.
impl XorTableTransform for Block {
    /// Applies the XOR table transformation on the `Block` using the provided `xor_table`.
    ///
    /// This method creates a new `Block` instance by applying the XOR table
    /// transformation on the current `Block` instance using the given `xor_table`.
    ///
    /// # Arguments
    ///
    /// * `xor_table` - The XOR table to be applied on the `Block` instance.
    ///
    /// # Returns
    ///
    /// * `output` - A new `Block` instance with the XOR table transformation applied.
    fn apply_xor_table(&self, xor_table: XorTable) -> Self {
        let mut output: Block = Block::default();
        for i in 0..(BITS_PER_WORD * WORDS_PER_BLOCK) {
            let mut bit: Bit = false;
            let mut local_index: usize = 0;
            while xor_table[i][local_index] != MARKER {
                bit = ((bit as u8) ^ (self.get_bit(xor_table[i][local_index]) as u8)) == 1;
                local_index += 1;
            }
            output.set_bit(i, bit);
        }
        output
    }
}

/// Implements the `LinearTransformation` trait for the `Block` struct.
///
/// The `LinearTransformation` trait provides methods to apply and invert
/// a linear transformation on a `Block` instance.
impl LinearTransformation for Block {
    /// Applies the inverted linear transformation on the `Block`.
    ///
    /// This method updates the current `Block` instance by applying the
    /// inverted linear transformation using the `XOR_TABLE_INVERSE`.
    fn apply_inverted_linear_transformation(&mut self) {
        *self = self.apply_xor_table(XOR_TABLE_INVERSE)
    }

    /// Applies the linear transformation on the `Block`.
    ///
    /// This method updates the current `Block` instance by applying the
    /// linear transformation using the `XOR_TABLE`.
    fn apply_linear_transformation(&mut self) {
        *self = self.apply_xor_table(XOR_TABLE)
    }
}

#[cfg(test)]
mod test_core {
    use super::*;

    const TEST_SUBKEYS: SubKeys = [
        [0x14072569, 0x9136ffda, 0x43f6395e, 0x4c68eb91],
        [0x31f4719, 0xe0efbb7f, 0xd742a8ec, 0xd006408a],
        [0x5b15a7f6, 0x249d424d, 0x501ed9e1, 0xe29bc3f],
        [0x37e0fa34, 0x61fa0ab9, 0xd87d02f, 0xd8b427a4],
        [0x64f1dc7, 0x63077ba6, 0x49536bd4, 0x431949e1],
        [0x915b50b6, 0x501bc85, 0x17336c20, 0x3d13df0d],
        [0x1701b4c5, 0x335d3759, 0x757ee62f, 0xea101d77],
        [0x9ccee62e, 0xebeb3eed, 0x6f4c065f, 0xacbfaa6f],
        [0xa72f303a, 0x631dfb79, 0x72658633, 0xb7ba4151],
        [0x11d75eb1, 0xc70e0a61, 0xdac49f93, 0x20634680],
        [0xe7034dea, 0xa822bc46, 0x28a10c06, 0x253de176],
        [0x48bf359c, 0x1b6753a1, 0xdda8516b, 0x4ba5f5d6],
        [0xe64b0389, 0x96d6f306, 0xe90c09bf, 0x1edea756],
        [0x93743c0a, 0x3173edbe, 0xed4488f7, 0xd9f93a8e],
        [0x4eff86ee, 0x1f8a905b, 0x74b4e4c0, 0x46fd695f],
        [0x28af3eee, 0xd1812c5c, 0xa51d5e27, 0xb732b8c5],
        [0xf0cb4132, 0x1a4a3b54, 0xb232dcf9, 0x3a535a35],
        [0x22350478, 0xfae11a54, 0xe6b5f74f, 0xb4be5204],
        [0x616baf47, 0xd7e1f085, 0x44883ab, 0xfe2eda7c],
        [0x727d7bc2, 0xe1371eb, 0xfc307547, 0x3027be94],
        [0xafa7ae54, 0x648250ec, 0xc99b1010, 0x58d72b0d],
        [0xb304912d, 0x16c746d, 0x8e6076c6, 0xe3c79c4e],
        [0x2da9760c, 0x6595434e, 0x905b1d01, 0x798ea463],
        [0x65537e8d, 0xc7a932a1, 0x6a888ab3, 0x892cda42],
        [0x594e189, 0xa45deee, 0x7a40b010, 0x1e0866a9],
        [0x3a7cfa4c, 0x5df6df4, 0x59631863, 0xcbc3309f],
        [0xfc6b9fed, 0xa8abebde, 0xafcca001, 0x47da0b55],
        [0x91d1fa23, 0x1d8e0e0f, 0xc79e8b6b, 0xdc8ae9e8],
        [0xcdd11bd2, 0x65968e39, 0xea804d01, 0x8639a25f],
        [0xa831679c, 0xb26aa2ce, 0xcb94cd6e, 0x5cc2e3b3],
        [0x742631ca, 0x2a5d4368, 0xb0d832d0, 0xeca8ecfc],
        [0xc982a64c, 0xbdfe62ec, 0xe9be985e, 0x4e39f71f],
        [0xcd03fc2e, 0xe4ea006, 0x157f874d, 0xb12447cb],
    ];

    const BAD_TEST_SUBKEYS: SubKeys = [
        [0x14072569, 0x9136ffda, 0x43f6395e, 0x4c68eb91],
        [0x31f4719, 0xe0efbb7f, 0xd742a8ec, 0xd006408a],
        [0x5b15a7f6, 0x249d424d, 0x501ed9e1, 0xe29bc3f],
        [0x37e0fa34, 0x61fa0ab9, 0xd87d02f, 0xd8b427a4],
        [0x64f1dc7, 0x63077ba6, 0x49536bd4, 0x431949e1],
        [0x915b50b6, 0x501bc85, 0x17336c20, 0x3d13df0d],
        [0x1701b4c5, 0x335d3759, 0x757ee62f, 0xea101d77],
        [0x9ccee62e, 0xebeb3eed, 0x6f4c065f, 0xacbfaa6f],
        [0xa72f303a, 0x631dfb79, 0x72658633, 0xb7ba4151],
        [0x11d75eb1, 0xc70e0a61, 0xdac49f93, 0x20634680],
        [0xe7034dea, 0xa822bc46, 0x28a10c06, 0x253de176],
        [0x48bf359c, 0x1b6753a1, 0xdda8516b, 0x4ba5f5d6],
        [0xe64b0389, 0x96d6f306, 0xe90c09bf, 0x1edea756],
        [0x93743c0a, 0x3173edbe, 0xed4488f7, 0xd9f93a8e],
        [0x4eff86ee, 0x1f8a905b, 0x74b4e4c0, 0x46fd695f],
        [0x28af3eee, 0xd1812c5c, 0xa51d5e27, 0xb732b8c5],
        [0xf0cb4132, 0x1a4a3b54, 0xb232dcf9, 0x3a535a35],
        [0x22350478, 0xfae11a54, 0xe6b5f74f, 0xb4be5204],
        [0x616baf47, 0xd7e1f085, 0x44883ab, 0xfe2eda7c],
        [0x727d7bc2, 0xe1371eb, 0xfc307547, 0x3027be94],
        [0xafa7ae54, 0x648250ec, 0xc99b1010, 0x58d72b0d],
        [0xb304912d, 0x16c746d, 0x8e6076c6, 0xe3c79c4e],
        [0x2da9760c, 0x6595434e, 0x905b1d01, 0x798ea463],
        [0x65537e8d, 0xc7a932a1, 0x6a888ab3, 0x892cda42],
        [0x594e189, 0xa45deee, 0x7a40b010, 0x1e0866a9],
        [0x3a7cfa4c, 0x5df6df4, 0x59631863, 0xcbc3309f],
        [0xfc6b9fed, 0xa8abebde, 0xafcca001, 0x47da0b55],
        [0x91d1fa23, 0x1d8e0e0f, 0xc79e8b6b, 0xdc8ae9e8],
        [0xcdd11bd2, 0x65968e39, 0xea804d01, 0x8639a25e],
        [0xa831679c, 0xb26aa2ce, 0xcb94cd6e, 0x5cc2e3b3],
        [0x742631ca, 0x2a5d4368, 0xb0d832d0, 0xeca8ecfc],
        [0xc982a64c, 0xbdfe62ec, 0xe9be985e, 0x4e39f71f],
        [0xcd03fc2e, 0xe4ea006, 0x157f874d, 0xb12447cd],
    ];

    /// Tests the left shift operation for `Block`.
    ///
    /// This test ensures that the `shift_block_left()` method correctly performs a left shift on the
    /// block's elements and handles carry bits as expected.
    #[test]
    fn shift_block_left() {
        let mut block = Block::new([1, 2, 3, 4]);
        let expected_block = Block::new([0x3, 0x4, 0x6, 0x8]);
        block.shift_block_left(true);

        assert_eq!(
            block, expected_block,
            "Blocks does not match when shifting left!"
        );
    }

    /// Tests the XOR operation for `Block`.
    ///
    /// This test ensures that the `^` operator correctly performs an XOR operation between two blocks.
    #[test]
    fn xor_ops_on_blocks() {
        let block_1: Block = Block::new([0, 1, 2, 3]);
        let block_2: Block = Block::new([3, 2, 1, 0]);
        let block_expected: Block = Block::new([3, 3, 3, 3]);

        assert_eq!(
            block_expected,
            block_1 ^ block_2,
            "Blocks does not match using XOR!"
        )
    }

    /// Tests the parallel S-box operation for `Block`.
    ///
    /// This test ensures that the `apply_parallel_sbox` method correctly applies the S-box operation in parallel to each nibble in the block.
    #[test]
    fn parallel_sbox_on_block() {
        let mut base_block = Block::new([0, 1, 2, 3]);
        base_block.apply_parallel_sbox(2);
        let expected_block = Block::new([2290649224, 2290649222, 2290649223, 2290649225]);

        assert_eq!(
            base_block, expected_block,
            "Blocks does not match using sboxes on block!"
        )
    }

    /// Parallel S-Box on Block Stress Test
    ///
    /// This test checks the correctness of the `apply_parallel_sbox` and
    /// `apply_inverted_parallel_sbox` methods for a `Block` under stress conditions.
    #[test]
    fn parallel_sbox_on_block_stress() {
        let base_block = Block::default();
        let mut block_2 = base_block.clone();
        for i in 0..1600 {
            block_2.apply_parallel_sbox(i as usize);
            block_2.apply_inverted_parallel_sbox(i as usize);
            assert_eq!(
                base_block, block_2,
                "Blocks does not match using sboxes on block!"
            );
        }
    }

    /// Tests the `apply_xor_table` operation on a `Block`.
    ///
    /// This test ensures that the `apply_xor_table` method correctly applies the XOR operation using the provided XOR table on a block.
    #[test]
    fn xor_table_on_block() {
        let block: Block = Block::new([0, 1, 2, 3]).apply_xor_table(XOR_TABLE);
        let expected_block: Block = Block::new([37830656, 2214596864, 54607904, 2164265248]);

        assert_eq!(
            block, expected_block,
            "Blocks does not match using XOR tables!"
        )
    }

    /// Linear Transform Block
    ///
    /// This test verifies the correctness of the `apply_linear_transformation` method for a `Block`.
    #[test]
    fn test_linear_tranform_block() {
        let mut block: Block = Block::new([0, 1, 2, 3]);
        block.apply_linear_transformation();
        let expected_block: Block = Block::new([37830656, 2214596864, 54607904, 2164265248]);

        assert_eq!(
            block, expected_block,
            "Blocks does not match using linear transformation!"
        )
    }

    /// Tests the `apply_round` method for a `Block`.
    #[test]
    fn apply_round_block() {
        let mut block: Block = Block::new([0, 1, 2, 3]);
        let keys: SubKeys = TEST_SUBKEYS.clone();
        block.apply_round(keys, 31);
        let expected_block: Block = Block::new([1501314759, 2905313109, 1104855236, 1411394578]);

        assert_eq!(block, expected_block, "Blocks does not match using round!")
    }

    /// Tests that applying a round and then its inverse results in the original `Block`.
    #[test]
    fn apply_round_and_reverse_it() {
        let base_block: Block = Block::new([0, 1, 2, 3]);
        let mut block: Block = base_block.clone();
        let keys: SubKeys = TEST_SUBKEYS.clone();
        block.apply_round(keys, 28);
        block.apply_inverted_round(keys, 28);

        assert_eq!(block, base_block, "Blocks does not match using rounds!")
    }

    /// Tests that applying a round and its inverse with wrong subkeys does not result in the original `Block`.
    #[test]
    fn apply_round_wrong_subkeys() {
        let base_block: Block = Block::new([0, 1, 2, 3]);
        let mut block: Block = base_block.clone();
        block.apply_round(TEST_SUBKEYS, 28);
        block.apply_inverted_round(BAD_TEST_SUBKEYS, 28);

        assert_ne!(block, base_block, "Blocks match using wrong subkeys!")
    }

    /// Tests that applying 33 rounds and then their inverses in reverse order results in the original `Block`.
    #[test]
    fn apply_round_and_reverse_it_stress() {
        let base_block: Block = Block::new([0, 1, 2, 3]);
        let mut block: Block = base_block.clone();
        let keys: SubKeys = TEST_SUBKEYS.clone();
        for i in 0..33 {
            block.apply_round(keys, i);
        }
        for i in (0..33).rev() {
            block.apply_inverted_round(keys, i);
        }

        assert_eq!(block, base_block, "Blocks does not match using rounds!")
    }

    /// Tests that applying the correct permutations in the correct order results in the original `Block`.
    #[test]
    fn apply_sbox_and_inverse_it() {
        let base_block: Block = Block::new([0, 1, 2, 3]);
        let mut block = base_block.clone();
        block.apply_permutation::<Initial>();
        block.apply_permutation::<Final>();
        block.apply_permutation::<FinalInverted>();
        block.apply_permutation::<InitialInverted>();
        assert_eq!(
            block, base_block,
            "Blocks does not match using permutations!"
        )
    }

    /// Tests that applying permutations and missing one does not result in the original `Block`.
    #[test]
    fn apply_sbox_and_inverse_it_missing_one() {
        let base_block: Block = Block::new([0, 1, 2, 3]);
        let mut block = base_block.clone();
        block.apply_permutation::<Final>();
        block.apply_permutation::<FinalInverted>();
        // block.apply_permutation::<InitialInverted>();
        block.apply_permutation::<Initial>();
        assert_ne!(block, base_block, "Blocks match using wrong permutations!")
    }

    /// Test the initial permutation of a block.
    #[test]
    fn test_block_initial_permutation() {
        let mut block: Block = Block::new([0x2A3B4C5D, 0x6E7F8A9B, 0x1C2D3E4F, 0x5A6B7C8D]);
        let expected_block: Block = Block::new([0xa503fd6f, 0x29ccfd60, 0xaf3f6bf, 0xa3cf6b0]);

        block.apply_permutation::<Initial>();
        assert_eq!(block, expected_block, "Block permutation error!")
    }

    /// Test the inverted initial permutation of a block.
    ///
    /// This test verifies that the `apply_permutation` method of the `Block` structure correctly
    /// applies the inverted initial permutation on the provided block. It compares the permuted block
    /// to the expected result.
    #[test]
    fn test_block_initial_permutation_inverted() {
        let mut block: Block = Block::new([0x2A3B4C5D, 0x6E7F8A9B, 0x1C2D3E4F, 0x5A6B7C8D]);
        let expected_block: Block = Block::new([0x99993333, 0x782df5f0, 0xad57f00f, 0x57555f55]);

        block.apply_permutation::<InitialInverted>();
        assert_eq!(block, expected_block, "Block permutation error!")
    }

    /// Test the final permutation of a block.
    ///
    /// This test verifies that the `apply_permutation` method of the `Block` structure correctly
    /// applies the final permutation on the provided block. It compares the permuted block
    /// to the expected result.
    #[test]
    fn test_block_final_permutation() {
        let mut block: Block = Block::new([0x2A3B4C5D, 0x6E7F8A9B, 0x1C2D3E4F, 0x5A6B7C8D]);
        let expected_block: Block = Block::new([0x99993333, 0x782df5f0, 0xad57f00f, 0x57555f55]);

        block.apply_permutation::<Final>();
        assert_eq!(block, expected_block, "Block permutation error!")
    }

    /// Test the inverted final permutation of a block.
    ///
    /// This test verifies that the `apply_permutation` method of the `Block` structure correctly
    /// applies the inverted final permutation on the provided block. It compares the permuted block
    /// to the expected result.
    #[test]
    fn test_block_final_permutation_inverted() {
        let mut block: Block = Block::new([0x2A3B4C5D, 0x6E7F8A9B, 0x1C2D3E4F, 0x5A6B7C8D]);
        let expected_block: Block = Block::new([0xa503fd6f, 0x29ccfd60, 0xaf3f6bf, 0xa3cf6b0]);

        block.apply_permutation::<FinalInverted>();
        assert_eq!(block, expected_block, "Block permutation error!")
    }
}

#[cfg(test)]
mod test_encryption {
    use super::*;

    const TEST_SUBKEYS: SubKeys = [
        [0x14072569, 0x9136ffda, 0x43f6395e, 0x4c68eb91],
        [0x31f4719, 0xe0efbb7f, 0xd742a8ec, 0xd006408a],
        [0x5b15a7f6, 0x249d424d, 0x501ed9e1, 0xe29bc3f],
        [0x37e0fa34, 0x61fa0ab9, 0xd87d02f, 0xd8b427a4],
        [0x64f1dc7, 0x63077ba6, 0x49536bd4, 0x431949e1],
        [0x915b50b6, 0x501bc85, 0x17336c20, 0x3d13df0d],
        [0x1701b4c5, 0x335d3759, 0x757ee62f, 0xea101d77],
        [0x9ccee62e, 0xebeb3eed, 0x6f4c065f, 0xacbfaa6f],
        [0xa72f303a, 0x631dfb79, 0x72658633, 0xb7ba4151],
        [0x11d75eb1, 0xc70e0a61, 0xdac49f93, 0x20634680],
        [0xe7034dea, 0xa822bc46, 0x28a10c06, 0x253de176],
        [0x48bf359c, 0x1b6753a1, 0xdda8516b, 0x4ba5f5d6],
        [0xe64b0389, 0x96d6f306, 0xe90c09bf, 0x1edea756],
        [0x93743c0a, 0x3173edbe, 0xed4488f7, 0xd9f93a8e],
        [0x4eff86ee, 0x1f8a905b, 0x74b4e4c0, 0x46fd695f],
        [0x28af3eee, 0xd1812c5c, 0xa51d5e27, 0xb732b8c5],
        [0xf0cb4132, 0x1a4a3b54, 0xb232dcf9, 0x3a535a35],
        [0x22350478, 0xfae11a54, 0xe6b5f74f, 0xb4be5204],
        [0x616baf47, 0xd7e1f085, 0x44883ab, 0xfe2eda7c],
        [0x727d7bc2, 0xe1371eb, 0xfc307547, 0x3027be94],
        [0xafa7ae54, 0x648250ec, 0xc99b1010, 0x58d72b0d],
        [0xb304912d, 0x16c746d, 0x8e6076c6, 0xe3c79c4e],
        [0x2da9760c, 0x6595434e, 0x905b1d01, 0x798ea463],
        [0x65537e8d, 0xc7a932a1, 0x6a888ab3, 0x892cda42],
        [0x594e189, 0xa45deee, 0x7a40b010, 0x1e0866a9],
        [0x3a7cfa4c, 0x5df6df4, 0x59631863, 0xcbc3309f],
        [0xfc6b9fed, 0xa8abebde, 0xafcca001, 0x47da0b55],
        [0x91d1fa23, 0x1d8e0e0f, 0xc79e8b6b, 0xdc8ae9e8],
        [0xcdd11bd2, 0x65968e39, 0xea804d01, 0x8639a25f],
        [0xa831679c, 0xb26aa2ce, 0xcb94cd6e, 0x5cc2e3b3],
        [0x742631ca, 0x2a5d4368, 0xb0d832d0, 0xeca8ecfc],
        [0xc982a64c, 0xbdfe62ec, 0xe9be985e, 0x4e39f71f],
        [0xcd03fc2e, 0xe4ea006, 0x157f874d, 0xb12447cb],
    ];

    const BAD_TEST_SUBKEYS: SubKeys = [
        [0x14072569, 0x9136ffdd, 0x43f6395e, 0x4c68eb91],
        [0x31f4719, 0xe0efbb7f, 0xd742a8ec, 0xd006408a],
        [0x5b15a7f6, 0x249d424d, 0x501ed9e1, 0xe29bc3f],
        [0x37e0fa34, 0x61fa0ab9, 0xd87d02f, 0xd8b427a4],
        [0x64f1dc7, 0x63077ba6, 0x49536bd4, 0x431949e1],
        [0x915b50b6, 0x501bc85, 0x17336c20, 0x3d13df0d],
        [0x1701b4c5, 0x335d3759, 0x757ee62f, 0xea101d77],
        [0x9ccee62e, 0xebeb3eed, 0x6f4c065f, 0xacbfaa6f],
        [0xa72f303a, 0x631dfb79, 0x72658633, 0xb7ba4151],
        [0x11d75eb1, 0xc70e0a61, 0xdac49f93, 0x20634680],
        [0xe7034dea, 0xa822bc43, 0x28a10c06, 0x253de176],
        [0x48bf359c, 0x1b6753a1, 0xdda8516b, 0x4ba5f5d6],
        [0xe64b0389, 0x96d6f306, 0xe90c09bf, 0x1edea756],
        [0x93743c0a, 0x3173edbe, 0xed4488f7, 0xd9f93a8e],
        [0x4eff86ee, 0x1f8a905b, 0x74b4e4c0, 0x46fd695f],
        [0x28af3eee, 0xd1812c5c, 0xa51d5e27, 0xb732b8c5],
        [0xf0cb4132, 0x1a4a3b54, 0xb232dcf9, 0x3a535a35],
        [0x22350478, 0xfae11a54, 0xe6b5f74f, 0xb4be5204],
        [0x616baf47, 0xd7e1f085, 0x44883ab, 0xfe2eda7c],
        [0x727d7bc2, 0xe1371eb, 0xfc307547, 0x3027be94],
        [0xafa7ae54, 0x648250ec, 0xc99b1010, 0x58d72b0d],
        [0xb304912d, 0x16c746d, 0x8e6076c6, 0xe3c79c4e],
        [0x2da9760c, 0x6595434e, 0x905b1d01, 0x798ea463],
        [0x65537e8d, 0xc7a932a1, 0x6a888ab3, 0x892cda42],
        [0x594e189, 0xa45deee, 0x7a40b010, 0x1e0866a9],
        [0x3a7cfa4c, 0x5df6df4, 0x59631863, 0xcbc3309f],
        [0xfc6b9fed, 0xa8abebde, 0xafcca001, 0x47da0b55],
        [0x91d1fa23, 0x1d8e0e0f, 0xc79e8b6b, 0xdc8ae9e8],
        [0xcdd11bd2, 0x65968e39, 0xea804d01, 0x8639a25e],
        [0xa831679c, 0xb26aa2ce, 0xcb94cd6e, 0x5cc2e3b3],
        [0x742631ca, 0x2a5d4368, 0xb0d832d0, 0xeca8ecfc],
        [0xc982a64c, 0xbdfe62ec, 0xe9be985e, 0x4e39f71f],
        [0xcd03fc2e, 0xe4ea006, 0x157f874d, 0xb12447cb],
    ];

    /// Test: Encrypt a block using a given set of subkeys (K_hat)
    ///
    /// This test verifies the correctness of the Serpent encryption process by comparing the encrypted result
    /// of a given block with an expected block. The test uses a known set of subkeys (`TEST_SUBKEYS`) and
    /// checks if the encryption process transforms the input block into the expected output block.
    #[test]
    fn encrypt_given_khat() {
        let mut block = Block::new([0, 1, 2, 3]);
        let expected_block = Block {
            words: [832019109, 1546749869, 23362685, 1260676003],
        };
        block.encrypt(&TEST_SUBKEYS);
        assert_eq!(block, expected_block, "Blocks are not encrypted correctly!");
    }

    /// Test: Decrypt a block using a given set of subkeys (K_hat)
    ///
    /// This test verifies the correctness of the Serpent decryption process by comparing the decrypted result
    /// of a given block with an expected block. The test uses a known set of subkeys (`TEST_SUBKEYS`) and
    /// checks if the decryption process transforms the input block into the expected output block.
    #[test]
    fn decrypt_given_khat() {
        let mut block = Block::new([0, 1, 2, 3]);
        let expected_block = Block {
            words: [4113610054, 1326800757, 3383764988, 2347762169],
        };
        block.decrypt(&TEST_SUBKEYS);
        assert_eq!(block, expected_block, "Blocks are not decrypted correctly!");
    }

    /// Test: Encrypt and decrypt a block using a given set of subkeys (K_hat)
    ///
    /// This test verifies the correctness of the Serpent encryption and decryption processes by
    /// encrypting and then decrypting a given block using a known set of subkeys (`TEST_SUBKEYS`).
    /// It checks if the decrypted result matches the original input block, ensuring that the
    /// encryption and decryption processes are inverse operations.
    #[test]
    fn enc_dec_khat() {
        let mut block = Block::new([0, 1, 2, 3]);
        let base_block = block.clone();
        block.encrypt(&TEST_SUBKEYS);
        block.decrypt(&TEST_SUBKEYS);
        assert_eq!(block, base_block, "Blocks does not match!");
    }

    /// Test: Encrypt and decrypt a block using different subkeys
    ///
    /// This test verifies the correctness of the Serpent encryption and decryption processes by
    /// encrypting a given block using a known set of subkeys (`TEST_SUBKEYS`) and then attempting to
    /// decrypt the encrypted block using a different set of subkeys (`BAD_TEST_SUBKEYS`). The decrypted
    /// result should not match the original input block, ensuring that the encryption and decryption
    /// processes are sensitive to the subkeys used.
    #[test]
    fn bad_key() {
        let mut block = Block::new([0, 1, 2, 3]);
        let base_block = block.clone();
        block.encrypt(&TEST_SUBKEYS);
        block.decrypt(&BAD_TEST_SUBKEYS);
        assert_ne!(
            block, base_block,
            "Encryption and decryption with different keys gave the same output!"
        );
    }
}

#[cfg(test)]
mod test_misc {
    use super::*;

    /// Tests the generation of random initialization vectors.
    ///
    /// This test checks that the generated initialization vector is not equal to an all-zero block.
    #[test]
    fn initialisation_vector_is_random() {
        assert_ne!(
            Block::initialization_vector(),
            Block::default(),
            "Initialisation vector is 0 !"
        )
    }

    /// Stress tests the generation of random initialization vectors.
    ///
    /// This test checks that the generated initialization vectors are not equal to each other, even when
    /// generating multiple vectors consecutively.
    #[test]
    fn initialisation_vector_is_random_stress() {
        let mut iv = Block::initialization_vector();
        let mut last = Block::default();

        for _ in 0..4096 {
            assert_ne!(
                iv, last,
                "Initialisation vector is identical to previous one!"
            );
            last = iv;
            iv = Block::initialization_vector();
        }
    }
}
