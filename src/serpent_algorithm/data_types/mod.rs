// NAGA - megaloadon - 2023

//! This module provides the data structures and types used in the Serpent cipher algorithm.
//!
//! # Data Structures
//!
//! * `block`: Contains the `Block` structure used to represent data blocks.
//! * `keys`: Contains the `Key`, `KeyInstance`, `SubKey`, and `SubKeys` structures used for key management.
//! * `nibble`: Contains the `Nibble` structure used to represent 4-bit data.
//! * `word`: Contains the `Word` structure used to represent 32-bit data.
//!
//! # Type Aliases
//!
//! * `Bit`: Represents a single bit (boolean value).
//! * `Nibble`: Represents a 4-bit value.
//! * `Word`: Represents a 32-bit value.
//! * `Block`: Represents a data block.
//! * `InitializationVector`: Represents an initialization vector (IV) for certain cipher modes.
//! * `KeyInstance`: Represents a key instance used for key scheduling.
//! * `SubKeys`: Represents a collection of subkeys.

mod block;
mod keys;
mod nibble;
mod word;

// Type alias (import shortcuts)
pub type Bit = bool;
pub type Nibble = nibble::Nibble;
pub type Word = word::Word;
pub type Block = block::Block;
pub type InitializationVector = Block;
pub type KeyInstance = keys::KeyInstance;
pub type SubKeys = keys::SubKeys;
