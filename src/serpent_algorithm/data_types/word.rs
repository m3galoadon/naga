// NAGA - megaloadon - 2023

use super::{
    super::{
        constants::{BITS_PER_NIBBLE, LAST_WORD_BIT},
        utils::{GetBit, SetBit},
    },
    nibble::{GetNibble, NibbleConversion},
    Bit, Nibble,
};

/// A type alias for a single word used in cryptographic operations.
///
/// The `Word` type is a 32-bit unsigned integer (u32) which represents
/// a fundamental unit of data in cryptographic algorithms.
pub type Word = u32;

/// Provides a custom implementation of the `SetBit` trait for the `Word` type.
///
/// This implementation allows setting a specific bit in a `Word` to either true
/// (1) or false (0) at the given position.
impl SetBit for Word {
    fn set_bit(&mut self, position: usize, bit: Bit) {
        let position = position.min(LAST_WORD_BIT);
        if bit {
            *self |= 1u32 << position;
        } else {
            *self &= !(1u32 << position);
        }
    }
}

/// Provides a custom implementation of the `GetBit` trait for the `Word` type.
///
/// This implementation allows retrieving the value of a specific bit in a `Word`
/// at the given position. The returned value is a `Bit`, which is a type alias
/// for a boolean value.
impl GetBit for Word {
    fn get_bit(&self, position: usize) -> Bit {
        let position = position.min(LAST_WORD_BIT);
        ((self & (1u32 << position)) >> position) != 0
    }
}

/// Provides a custom implementation of the `GetNibble` trait for the `Word` type.
///
/// This implementation allows retrieving a 4-bit `Nibble` from a `Word` at the
/// given position. The returned value is a `Nibble`, which is an array of four
/// `Bit` values.
impl GetNibble for Word {
    fn get_nibble(&self, position: usize) -> Nibble {
        Nibble::from_u(0xf & (*self >> (position * BITS_PER_NIBBLE)))
    }
}

#[cfg(test)]
mod test {
    use super::*;

    /// Test the correct retrieval of a `Nibble` from a `Word`.
    ///
    /// This test checks if the correct `Nibble` is retrieved from a `Word` at the specified position.
    #[test]
    fn test_get_nibble_from_word() {
        let base_word: Word = 5;
        let expected_nibble: Nibble = Nibble::from_u::<u8>(0);
        let nibble: Nibble = base_word.get_nibble(2);
        assert_eq!(nibble, expected_nibble, "Failed to get nibble from word!")
    }

    /// Test the correct retrieval of a bit from a `Word`.
    ///
    /// This test checks if the correct bit is retrieved from a `Word` at the specified positions.
    #[test]
    fn test_get_bit() {
        let word: Word = Word::from_le_bytes([0x12, 0x34, 0x56, 0x78]);

        assert_eq!(word.get_bit(0), false, "Wrong Bit!");
        assert_eq!(word.get_bit(4), true, "Wrong Bit!");
    }

    /// Test the correct setting of a bit in a `Word`.
    ///
    /// This test checks if the correct bit is set in the `Word` at the specified positions and
    /// ensures that the modified `Word` matches the expected result.
    #[test]
    fn test_set_bit() {
        let word: Word = Word::from_le_bytes([0x12, 0x34, 0x56, 0x78]);
        let mut word_to_modify: Word = Word::from_le_bytes([0x12, 0x34, 0x56, 0x78]);
        let expected_word: Word = Word::from_le_bytes([0x3, 0x34, 0x56, 0x78]);

        word_to_modify.set_bit(0, true);
        word_to_modify.set_bit(4, false);

        // Test it
        assert_ne!(word_to_modify, word, "Words should not be the same!");
        assert_eq!(word_to_modify, expected_word, "Words, should be the same!");
    }
}
