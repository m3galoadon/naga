// NAGA - megaloadon - 2023

use super::{
    constants::WORDS_PER_BLOCK,
    data_types::{Bit, Block},
};
use rand::{thread_rng, RngCore};

use zeroize::Zeroize;

/// A trait for getting the value of a specific bit within a type.
pub trait GetBit {
    /// Returns the bit value (`Bit`) at the given position `usize`.
    fn get_bit(&self, position: usize) -> Bit;
}

/// A trait for setting the value of a specific bit within a mutable type.
pub trait SetBit {
    /// Sets the bit value (`Bit`) at the given position `usize`.
    fn set_bit(&mut self, position: usize, bit: Bit);
}

/// A trait for converting a type into a `Vec<Block>`.
pub trait ToBlocks {
    /// Converts the type into a vector of `Block`s.
    /// If `add_padding` is `true`, padding will be added to the data before the conversion.
    fn to_blocks(&self, add_padding: bool) -> Vec<Block>;
}

/// A trait for converting a `Vec<Block>` into a type.
pub trait FromBlocks {
    /// Converts a vector of `Block`s into the desired type.
    /// If `remove_padding` is `true`, padding will be removed from the data after the conversion.
    fn from_blocks(blocks: Vec<Block>, remove_padding: bool) -> Self;
}

/// Implementation of `ToBlocks` for `Vec<u8>`.
///
/// This implementation allows converting a `Vec<u8>` into a vector of `Block`. It optionally adds padding to the end of the data to ensure the data size is a multiple of
impl ToBlocks for Vec<u8> {
    fn to_blocks(&self, add_padding: bool) -> Vec<Block> {
        let mut result: Vec<Block> = vec![];
        let mut self_data = self.clone();

        // Add padding
        if add_padding {
            let padding_size = 16 - (self_data.len() % 16);
            for _ in 0..padding_size {
                self_data.push(padding_size as u8);
            }
        }

        // Go through all blocks of [u8;16]
        for i in 0..(self_data.len() / 16) {
            // Build [u8;16] from &[u8]
            let mut actual_block_as_u8 = [0u8; 16];
            for j in 0..16 {
                actual_block_as_u8[j] = self_data[(i * 16) + j];
            }

            // Add it to result
            result.push(Block::from(actual_block_as_u8))
        }

        result
    }
}

/// Implementation of `FromBlocks` for `Vec<u8>`.
///
/// This implementation allows converting a vector of `Block` back into a `Vec<u8>`. It optionally removes padding
impl FromBlocks for Vec<u8> {
    fn from_blocks(blocks: Vec<Block>, remove_padding: bool) -> Self {
        // Init result
        let mut result: Vec<u8> = vec![];

        // convert it
        for i in 0..blocks.len() {
            let mut inner_result = [0u8; 16];
            for j in 0..WORDS_PER_BLOCK {
                inner_result[j * 4] = (blocks[i][j]) as u8;
                inner_result[j * 4 + 1] = (blocks[i][j] >> 8) as u8;
                inner_result[j * 4 + 2] = (blocks[i][j] >> 16) as u8;
                inner_result[j * 4 + 3] = (blocks[i][j] >> 24) as u8;
            }

            result.extend(inner_result);
            inner_result.zeroize();
        }

        // Remove padding
        if remove_padding {
            for _ in 0..(*result.last().unwrap()) {
                result.pop();
            }
        }

        result
    }
}

/// Generates a random salt of the given size.
///
/// `size`: The length of the salt to be generated, in bytes.
///
/// # Returns
/// A `Vec<u8>` containing the generated salt of the specified size.
pub fn generate_salt(size: usize) -> Vec<u8> {
    let mut salt = vec![0u8; size];
    thread_rng().fill_bytes(&mut salt);
    salt
}

#[cfg(test)]
mod test {
    use crate::serpent_algorithm::constants::SALT_LEN;

    use super::*;

    // Turn bytes into blocks and back again to data to test ToBlock and FromBlock
    // traits implementations for u8
    #[test]
    fn bytes_to_block_and_back_to_data() {
        let base_data: Vec<u8> = b"hello world!".to_vec();
        assert_eq!(
            base_data,
            Vec::<u8>::from_blocks(base_data.to_blocks(true), true),
            "Failed to convert bytes to block and back to bytes!"
        )
    }

    /// This test checks that the `generate_salt` function correctly generates salt values of various sizes.
    /// By iterating through a range of salt sizes from 0 to 4096, the test ensures that the length of the
    /// generated salt matches the specified input length.
    #[test]
    fn stress_salt_size() {
        (0..4096).for_each(|i| {
            assert_eq!(
                generate_salt(i).len(),
                i,
                "Salt did not generate the specified length!"
            )
        })
    }

    /// This test checks that the `generate_salt` function generates unique salt values.
    /// By generating salts 4096 times, the test ensures that none of the generated salts are
    /// identical. This is important for ensuring the uniqueness of salts in cryptographic applications.
    #[test]
    fn stress_salt_different() {
        let mut salts: Vec<Vec<u8>> = vec![];
        (0..4096).for_each(|_| {
            let lsalt = generate_salt(SALT_LEN);
            assert_eq!(
                false,
                salts.contains(&lsalt),
                "Salt had already been generated!"
            );
            salts.push(lsalt);
        })
    }

    /// This test checks the conversion between data and blocks using the `to_blocks` and `from_blocks` methods.
    /// By generating salts of varying lengths (0 to 4095), the test ensures that the conversion process between
    /// data and blocks is consistent and that no data is lost during the conversions.
    #[test]
    fn stress_from_and_to_blocks() {
        (0..4096).for_each(|i| {
            let data = generate_salt(i);
            let blocks = data.to_blocks(true);
            assert_eq!(
                data,
                Vec::<u8>::from_blocks(blocks, true),
                "Blocks conversion failed and data has been lost!"
            );
        });
    }
}
