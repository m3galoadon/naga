// NAGA - megaloadon - 2023

/// A marker constant used for various purposes.
pub const MARKER: usize = 0xff;

/// Length of the initialization vector (IV) in bytes.
pub const IV_LEN: usize = 16;

/// Length of the salt in bytes.
pub const SALT_LEN: usize = 32;

/// Number of bits in a word.
pub const BITS_PER_WORD: usize = 32;

/// Index of the last bit in a word.
pub const LAST_WORD_BIT: usize = BITS_PER_WORD - 1;

/// Number of words in a block.
pub const WORDS_PER_BLOCK: usize = 4;

/// Index of the last bit in a nibble.
pub const LAST_NIBBLE_BIT: usize = 3;

/// Number of bits in a nibble.
pub const BITS_PER_NIBBLE: usize = LAST_NIBBLE_BIT + 1;

/// Size of a block in bits.
pub const BLOCK_SIZE: usize = WORDS_PER_BLOCK * BITS_PER_WORD;

/// Number of nibbles in a word.
pub const NIBBLES_PER_WORD: usize = BITS_PER_WORD / BITS_PER_NIBBLE;

/// Number of rounds.
pub const ROUNDS: usize = 32;

/// Constant value used in Serpent Cipher
pub const PHI: u32 = 0x9e3779b9;
