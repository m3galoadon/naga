// NAGA - megaloadon - 2023

mod serpent_algorithm;

use serpent_algorithm::cipher_instance::{CipherInstance, CipherMode};
use zeroize::Zeroize;

/// An enumeration representing the available block cipher modes of operation
/// for the Serpent cipher.
///
/// The block cipher modes of operation determine how the Serpent cipher
/// algorithm is applied to a sequence of plaintext blocks during the encryption
/// and decryption processes.
///
/// # Variants
///
/// * `ECB`: Electronic Codebook (ECB) mode. In this mode, each block of plaintext
///   is encrypted independently with the same key. It is the simplest mode
///   and may reveal patterns in the plaintext when used with repetitive data.
///
/// * `CBC`: Cipher Block Chaining (CBC) mode. In this mode, each block of plaintext
///   is XORed with the previous ciphertext block before being encrypted.
///   This mode provides better security than ECB, as patterns in the plaintext
///   are concealed.
///
/// * `CFB1`: Cipher Feedback (CFB) mode with 1-bit feedback. In this mode,
///   the previous ciphertext block is encrypted and then XORed with the
///   current plaintext block to produce the ciphertext block. The 1-bit
///   feedback means that only 1 bit of the ciphertext is fed back into the
///   encryption process, providing fine-grained control over the data.
#[derive(Debug, Zeroize, Clone)]
pub enum CipherModeInit {
    ECB,
    CBC,
    CFB1,
    CTR,
}

impl CipherModeInit {
    /// Converts the `CipherModeInit` enum variant to its corresponding `CipherMode` enum variant.
    ///
    /// This function is used to convert between the high-level `CipherModeInit` enumeration,
    /// which represents the available block cipher modes of operation for the Serpent cipher,
    /// and the internal `CipherMode` enumeration, which is used for the actual implementation
    /// of the cipher modes.
    ///
    /// # Returns
    ///
    /// A `CipherMode` enum variant that corresponds to the `CipherModeInit` variant.
    fn to_internal(&self) -> CipherMode {
        match &self {
            CipherModeInit::ECB => CipherMode::ECB,
            CipherModeInit::CBC => CipherMode::CBC,
            CipherModeInit::CFB1 => CipherMode::CFB1,
            CipherModeInit::CTR => CipherMode::CTR,
        }
    }
}

/// Provides a default implementation for the `CipherModeInit` enum.
///
/// This implementation sets the default block cipher mode to CBC (Cipher Block Chaining).
impl Default for CipherModeInit {
    fn default() -> Self {
        CipherModeInit::CBC
    }
}

/// Implements the `Drop` trait for the `CipherModeInit` enum.
///
/// This implementation ensures that the memory used by the enum is securely erased
/// when it goes out of scope. It does this by calling the `zeroize` method on the enum.
impl Drop for CipherModeInit {
    fn drop(&mut self) {
        self.zeroize();
    }
}

/// The `NagaEngine` struct represents a high-level API for working with the Serpent cipher.
///
/// This struct allows users to select and interact with different block cipher modes,
/// such as ECB, CBC, or CFB1, through the `CipherModeInit` enum.
pub struct NagaEngine {
    mode: CipherModeInit,
}

impl NagaEngine {
    /// Creates a new `NagaEngine` with the specified block cipher mode.
    pub fn new(mode: CipherModeInit) -> NagaEngine {
        NagaEngine { mode }
    }

    /// Encrypts the given data using the selected block cipher mode and the provided password.
    pub fn encrypt(&self, data: &mut Vec<u8>, password: &[u8]) {
        let mut cipherngn = CipherInstance::new(self.mode.to_internal(), password);
        if let Err(e) = cipherngn.encrypt(data) {
            eprintln!("An error occured : {e}");
        };
    }

    /// Decrypts the given data using the selected block cipher mode and the provided password.
    pub fn decrypt(&self, data: &mut Vec<u8>, password: &[u8]) {
        let mut cipherngn = CipherInstance::new(self.mode.to_internal(), password);
        if let Err(e) = cipherngn.decrypt(data) {
            eprintln!("An error occured : {e}");
        };
    }
}

impl Default for NagaEngine {
    fn default() -> Self {
        NagaEngine {
            mode: CipherModeInit::default(),
        }
    }
}

#[cfg(test)]
mod api {
    use super::*;

    #[test]
    fn test_api_default() {
        let base_data: Vec<u8> = vec![0, 1, 2, 3, 4, 5, 6];
        let password = "hello world!".as_bytes();
        let mut data = base_data.clone();
        let ngn = NagaEngine::default();
        ngn.encrypt(&mut data, password);
        ngn.decrypt(&mut data, password);
        assert_eq!(data, base_data, "Data not recovered by default API! (cbc)")
    }

    #[test]
    fn test_api_ecb() {
        let base_data: Vec<u8> = vec![0, 1, 2, 3, 4, 5, 6];
        let password = "hello world!".as_bytes();
        let mut data = base_data.clone();
        let ngn = NagaEngine::new(CipherModeInit::ECB);
        ngn.encrypt(&mut data, password);
        ngn.decrypt(&mut data, password);
        assert_eq!(data, base_data, "Data not recovered by default API! (cbc)")
    }

    #[test]
    fn test_api_cbc() {
        let base_data: Vec<u8> = vec![0, 1, 2, 3, 4, 5, 6];
        let password = "hello world!".as_bytes();
        let mut data = base_data.clone();
        let ngn = NagaEngine::new(CipherModeInit::CBC);
        ngn.encrypt(&mut data, password);
        ngn.decrypt(&mut data, password);
        assert_eq!(data, base_data, "Data not recovered by default API! (cbc)")
    }

    #[test]
    fn test_api_cfb1() {
        let base_data: Vec<u8> = vec![0, 1, 2, 3, 4, 5, 6];
        let password = "hello world!".as_bytes();
        let mut data = base_data.clone();
        let ngn = NagaEngine::new(CipherModeInit::CFB1);
        ngn.encrypt(&mut data, password);
        ngn.decrypt(&mut data, password);
        assert_eq!(data, base_data, "Data not recovered by default API! (cbc)")
    }

    #[test]
    fn test_api_ctr() {
        let base_data: Vec<u8> = vec![0, 1, 2, 3, 4, 5, 6];
        let password = "hello world!".as_bytes();
        let mut data = base_data.clone();
        let ngn = NagaEngine::new(CipherModeInit::CTR);
        ngn.encrypt(&mut data, password);
        ngn.decrypt(&mut data, password);
        assert_eq!(data, base_data, "Data not recovered by default API! (cbc)")
    }
}
