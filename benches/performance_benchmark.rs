use criterion::{black_box, criterion_group, criterion_main, Criterion};
use naga::NagaEngine;

/// Simple performance benchmark for encryption and decryption of ECB mode.
fn base_benchmark_ecb(c: &mut Criterion) {
    let mut data = black_box(b"Some benchmarking random data!".to_vec());
    let pass = black_box(b"Password123");

    c.bench_function("ECB-base-bench", |b| {
        b.iter(|| {
            NagaEngine::new(naga::CipherModeInit::ECB).encrypt(&mut data, pass);
            NagaEngine::new(naga::CipherModeInit::ECB).decrypt(&mut data, pass);
        })
    });
}

/// Simple performance benchmark for encryption and decryption of CBC mode.
fn base_benchmark_cbc(c: &mut Criterion) {
    let mut data = black_box(b"Some benchmarking random data!".to_vec());
    let pass = black_box(b"Password123");

    c.bench_function("CBC-base-bench", |b| {
        b.iter(|| {
            NagaEngine::new(naga::CipherModeInit::CBC).encrypt(&mut data, pass);
            NagaEngine::new(naga::CipherModeInit::CBC).decrypt(&mut data, pass);
        })
    });
}

/// Simple performance benchmark for encryption and decryption of CFB1 mode.
fn base_benchmark_cfb1(c: &mut Criterion) {
    let mut data = black_box(b"Some benchmarking random data!".to_vec());
    let pass = black_box(b"Password123");

    c.bench_function("CFB1-base-bench", |b| {
        b.iter(|| {
            NagaEngine::new(naga::CipherModeInit::CFB1).encrypt(&mut data, pass);
            NagaEngine::new(naga::CipherModeInit::CFB1).decrypt(&mut data, pass);
        })
    });
}

/// Simple performance benchmark for encryption and decryption of CTR mode.
fn base_benchmark_ctr(c: &mut Criterion) {
    let mut data = black_box(b"Some benchmarking random data!".to_vec());
    let pass = black_box(b"Password123");

    c.bench_function("CTR-base-bench", |b| {
        b.iter(|| {
            NagaEngine::new(naga::CipherModeInit::CTR).encrypt(&mut data, pass);
            NagaEngine::new(naga::CipherModeInit::CTR).decrypt(&mut data, pass);
        })
    });
}

criterion_group!(
    benches,
    base_benchmark_ecb,
    base_benchmark_cbc,
    base_benchmark_cfb1,
    base_benchmark_ctr
);

criterion_main!(benches);
