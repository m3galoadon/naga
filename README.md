# NAGA
Nervous Agent Guardian Algorithm

*Serpent Cipher Rust Implementation*

## Description
NAGA is a Rust lib to encrypt and decrypt data using the Serpent Cipher Algorithm.

## Installation and usage
*Into your rust project*
```
git clone https://gitlab.com/m3galoadon/naga
cargo add naga --path ./naga
```

*Into your program*
```
// Base data
let data: Vec<u8> = vec![0, 1, 2, 3, 4, 5, 6];

// Password
let password = "hello world!".as_bytes();

// Cipher Engine
let ngn = NagaEngine::default();

// Encrypt
ngn.encrypt(&mut data, password);

// Decrypt
ngn.decrypt(&mut data, password);
```

## License
MIT, see **LICENSE** file for more informations.